package tests

import (
	"bytes"
	"crypto/rand"
	"gitlab.com/data-custodian/custodian-go/models"
	"custodian/audit-trail/tests/config"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"
	"time"

	"github.com/golang-jwt/jwt"
)

const (
	testUser        string = "mytestuser"
	testActor       string = "mytestactor"
	nonExistentUser string = "nonexistentuser"
)

func randomString() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func generateEvent(timestamp int64, nonce string) (custodian.Event, error) {
	err := config.ReadConfig()
	if err != nil {
		return custodian.Event{}, err
	}

	claims := custodian.TokenClaims{
		UserID:      testUser,
		ClientID:    "myclientid",
		SignatureID: "",
		Actor:       testActor,
		Scope:       nil,
		Constraints: nil,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
			Issuer:    "mf.testing.swisscustodian.ch", // FIXME: hardcoded
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["kid"] = config.C.Kid // FIXME: change config key name
	signedToken, err := token.SignedString(config.C.PrivateKey)

	event := custodian.Event{
		Unix:     timestamp,
		Token:    signedToken,
		ID:       "my:event:id",
		Resource: "my:resource",
		Action:   "my:action",
		Attribute: map[string]interface{}{
			"actor": testActor,
			"owner": testUser,
			"nonce": nonce,
		},
	}
	return event, nil
}

func containsEvent(events []custodian.Event, timestamp int64, nonce string) bool {
	for _, event := range events {
		if event.Unix == timestamp && event.Attribute["nonce"] == nonce {
			return true
		}
	}
	return false
}

func TestGetEvents(t *testing.T) {
	err := config.ReadConfig()
	if err != nil {
		t.Fatalf(`Error: %s`, err)
	}

	resp, err := http.Get(config.C.Url + "/events?userid=" + testUser)
	if err != nil {
		t.Fatalf(`Error: %s`, err.Error())
	}

	var events []custodian.Event

	err = json.NewDecoder(resp.Body).Decode(&events)
	if err != nil {
		log.Printf(`Error: %s`, err.Error())
	}
}

func TestGetEventsNonExistentUser(t *testing.T) {
	err := config.ReadConfig()
	if err != nil {
		t.Fatalf(`Error: %s`, err)
	}

	resp, err := http.Get(config.C.Url + "/events?userid=" + nonExistentUser)
	if err != nil {
		t.Fatalf(`Error: %s`, err.Error())
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Fatalf(`Error: %s`, "non existent user should return 404")
	}
}

func TestPostEvent(t *testing.T) {
	err := config.ReadConfig()
	if err != nil {
		t.Fatalf(`Error: %s`, err)
	}

	timestamp := time.Now().Unix()
	nonce := randomString()
	event, err := generateEvent(timestamp, nonce)
	if err != nil {
		t.Fatalf(`Error: %s`, err.Error())
	}

	eventJson, err := json.Marshal(event)
	if err != nil {
		t.Fatalf(`Error: %s`, err.Error())
	}
	resp, err := http.Post(config.C.Url+"/event", "application/json", bytes.NewBuffer(eventJson))
	if err != nil {
		t.Fatalf(`Error: %s`, err.Error())
	} else if resp.StatusCode != http.StatusOK {
		t.Fatalf(`Error: %s`, "post event should return 200")
	}

	// wait for event to be processed
	time.Sleep(time.Second * 5)

	resp, err = http.Get(config.C.Url + "/events?userid=" + testUser)
	if err != nil {
		t.Fatalf(`Error: %s`, err.Error())
	}

	var events []custodian.Event

	err = json.NewDecoder(resp.Body).Decode(&events)
	if err != nil {
		log.Printf(`Error: %s`, err.Error())
	}

	if !containsEvent(events, timestamp, nonce) {
		t.Fatalf(`Error: %s`, "could not retrieve event")
	}
}
