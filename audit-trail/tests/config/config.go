package config

import (
	"crypto/ecdsa"
	"gitlab.com/data-custodian/custodian-go/util"
	"strings"

	"github.com/spf13/viper"
)

type config struct {
	// Location of the ECDSA Private Key
	PrivateKeyLocation string `yaml:"PrivateKeyLocation"`

	// ECDSA Private Key (loaded from PrivateKeyLocation)
	PrivateKey *ecdsa.PrivateKey

	// The key id of the ECDSA Private Key
	Kid string `yaml:"Kid"`

	// URL of the EPP service
	Url string `yaml:"URL"`
}

var C config

func ReadConfig() error {
	Config := &C
	viper.SetConfigName("config") // name of the config file
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if err := viper.Unmarshal(&Config); err != nil {
		return err
	}

	privateKey, err := util.ReadECDSAPrivateKey(Config.PrivateKeyLocation)
	if err != nil {
		return err
	}
	Config.PrivateKey = privateKey

	return nil
}
