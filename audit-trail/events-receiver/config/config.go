/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"strings"

	"github.com/spf13/viper"
)

type config struct {
	// Config struct for webapp config
	Server server `yaml:"Server"`

	// Config struct for Pulsar
	Pulsar messageQueue `yaml:"Pulsar"`
}

type server struct {
	//	hostname string `yaml:"hostname"`
	Hostname string `yaml:"hostname"`
	//	host string `yaml:"port"`
	Port string `yaml:"port"`
	// URL path containing the component name (e.g., "/accesscontrol/pdp")
	ComponentUrlPath string `yaml:"ComponentUrlPath"`
}

type messageQueue struct {
	// Pulsar URL
	URL string `yaml:"url"`
	// Pulsar topics
	Topics topics `yaml:"topics"`
	// Messages keys
	Keys keys `yaml:"keys"`
}

type topics struct {
	Events string `yaml:"events"`
}

type keys struct {
	Event string `yaml:"event"`
}

// We set a default values in case config.yml is not configured
// These default values will be replaced by values configured in config/config.yml
var C config = config{
	Server: server{"localhost", "8015", ""},
}

func ReadConfig() error {
	Config := &C
	viper.SetConfigName("config") // name of the config file
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if err := viper.Unmarshal(&Config); err != nil {
		return err
	}
	// remove after just for testing
	//spew.Dump(C)
	return nil
}
