package receiver

import (
	"bytes"
	"custodian/audit-trail/events-receiver/config"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	custodian "gitlab.com/data-custodian/custodian-go/models"
)

func sendPostEvent(event custodian.Event) string {
	jsonEvent, err := json.Marshal(event)
	if err != nil {
		return ""
	}

	req := httptest.NewRequest(http.MethodPost, "/audit-trail/event", bytes.NewReader(jsonEvent))
	w := httptest.NewRecorder()

	PostEvent(w, req)

	resp := w.Result()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return ""
	}
	return strings.TrimSpace(string(body))
}

func setTestConfig() {
	config.C.Server.Hostname = "localhost"
	config.C.Server.Port = "8015"
	config.C.Server.ComponentUrlPath = ""
	config.C.Pulsar.URL = "test://invalid_url" // invalid URL set on purpose to avoid connecting to Pulsar
	config.C.Pulsar.Topics.Events = "events"
	config.C.Pulsar.Keys.Event = "event"
}

func TestPostEvent(t *testing.T) {
	setTestConfig()

	req := httptest.NewRequest(http.MethodPost, "/audit-trail/event", nil)
	w := httptest.NewRecorder()
	PostEvent(w, req)
	resp := w.Result()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("error reading body: %v", err)
	} else if strings.TrimSpace(string(body)) != "Invalid query" {
		t.Errorf("expected body to be %s, got %s", "Invalid query", string(body))
	}

	var testEvent custodian.Event

	bodyString := sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Missing timestamp" {
		t.Errorf("expected body to be %s, got %s", "Missing timestamp", bodyString)
	}

	testEvent.Unix = 1
	bodyString = sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Missing Token" {
		t.Errorf("expected body to be %s, got %s", "Missing Token", bodyString)
	}

	testEvent.Token = "token"
	bodyString = sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Missing ID" {
		t.Errorf("expected body to be %s, got %s", "Missing ID", bodyString)
	}

	testEvent.ID = "id"
	bodyString = sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Missing Resource" {
		t.Errorf("expected body to be %s, got %s", "Missing Resource", bodyString)
	}

	testEvent.Resource = "resource"
	bodyString = sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Missing Action" {
		t.Errorf("expected body to be %s, got %s", "Missing Action", bodyString)
	}

	testEvent.Action = "action"
	bodyString = sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Missing Attribute" {
		t.Errorf("expected body to be %s, got %s", "Missing Attribute", bodyString)
	}

	// Valid event
	// The response must be an error saying that the Pulsar URL is invalid
	testEvent.Attribute = make(map[string]interface{})
	bodyString = sendPostEvent(testEvent)
	if bodyString == "" || bodyString != "Invalid URL scheme 'test': InvalidConfiguration" {
		t.Errorf("expected body to be %s, got %s", "Invalid URL scheme 'test': InvalidConfiguration", bodyString)
	}
}
