/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package receiver

import (
	"custodian/audit-trail/events-receiver/config"
	"encoding/json"
	"log"
	"net/http"

	mqconnector "gitlab.com/data-custodian/custodian-go/connectors/mq"
	custodian "gitlab.com/data-custodian/custodian-go/models"
)

var (
	// To use with Pulsar
	mqProducer mqconnector.PulsarConnector

	// To use with Kafka
	/*
		mqProducer mqconnector.KafkaConnector = mqconnector.KafkaConnector{
		    KAFKA_URL,
		    10*time.Second,
		}
	*/
)

// PostEvent adds an JSON event to the message queue.
//
// The request body format must be JSON and must respect the Event structure.
func PostEvent(w http.ResponseWriter, r *http.Request) {
	var event custodian.Event
	err := json.NewDecoder(r.Body).Decode(&event)
	if err != nil {
		http.Error(w, "Invalid query", http.StatusBadRequest)
		return
	}

	if event.Unix == 0 {
		http.Error(w, "Missing timestamp", http.StatusBadRequest)
		return
	}
	if event.Token == "" {
		http.Error(w, "Missing Token", http.StatusBadRequest)
		return
	}
	if event.ID == "" {
		http.Error(w, "Missing ID", http.StatusBadRequest)
		return
	}
	if event.Resource == "" {
		http.Error(w, "Missing Resource", http.StatusBadRequest)
		return
	}
	if event.Action == "" {
		http.Error(w, "Missing Action", http.StatusBadRequest)
		return
	}
	if event.Attribute == nil {
		http.Error(w, "Missing Attribute", http.StatusBadRequest)
		return
	}

	mqProducer = mqconnector.PulsarConnector{
		URL: config.C.Pulsar.URL,
	}

	jsonEvent, err := json.Marshal(event)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = mqProducer.SendMessage(r.Context(), jsonEvent, config.C.Pulsar.Keys.Event, config.C.Pulsar.Topics.Events, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)

	log.Println("New event:", event)

	return
}
