# Event Processing Point Endpoint

The EPP event receives events and adds them to the message queue.


## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

### Configuration

The config file can be found in `config/config.yml`. For the new configuration to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in the `custodian/audit-trail/events-receiver` folder and run the following command:
```
go build -o events-receiver code/main.go
./events-receiver
```

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the `custodian/audit-trail/events-receiver` folder or one of its subfolders, and run the following command:

```
godoc -http=localhost:6060
```

The documentation for the structures and interfaces is accessible at [http://localhost:6060/pkg/custodian/audit-trail/events-receiver/code/go/](http://localhost:6060/pkg/custodian/audit-trail/events-receiver/code/go/)

