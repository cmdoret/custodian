/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"context"
	"crypto/ecdsa"
	"log"
	"net/http"
	"time"

	epp "custodian/audit-trail/events-endpoint/code/go"
	"custodian/audit-trail/events-endpoint/config"
	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	"gitlab.com/data-custodian/custodian-go/models"
)

func main() {
	ctx := context.Background()

	epp.Events = make(map[string][]custodian.Event)
	epp.Usernames = make(map[string]string)
	epp.PublicKeys = make(map[string]*ecdsa.PublicKey)

	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	log.Println(config.C)

	epp.MongoPK = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.PublicKeys,
	)
	defer epp.MongoPK.CloseClientDB(ctx)

	// We read the messages from the MQ in parallel
	go epp.ReadMessages(ctx)

	time.Sleep(2 * time.Second)
	log.Println("Loaded events:", epp.Events)

	router := epp.NewRouter()
	log.Println("Server is running at " + config.C.Server.Port + " port.")
	log.Fatal(http.ListenAndServe(config.C.Server.Hostname+":"+config.C.Server.Port, router))
}
