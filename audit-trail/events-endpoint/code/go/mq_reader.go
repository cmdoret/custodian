/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package epp

import (
	"context"
	"crypto/ecdsa"
	"encoding/json"
	"log"
	"time"

	"custodian/audit-trail/events-endpoint/config"
	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	"gitlab.com/data-custodian/custodian-go/models"
	"gitlab.com/data-custodian/custodian-go/util"

	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type publicKeyStructure struct {
	UserID string `json:"userid" bson:"userid"`
	Kid    string `json:"kid" bson:"kid"`
	PK     string `json:"pk" bson:"pk"`
}

var (
	MongoPK    mongofunc.MongoConnection
	PublicKeys map[string]*ecdsa.PublicKey
	Usernames  map[string]string // FIXME: small hack to resolve usernames
)

// ReadMessages subscribes to the message queue and interprets the messages to recreate a view of the events
func ReadMessages(ctx context.Context) {
	msgChannel := make(chan pulsar.ConsumerMessage)

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: config.C.PulsarInput.URL,
	})

	if err != nil {
		log.Fatalf("Could not create client: %v", err)
	}

	pulsarConfig := pulsar.ConsumerOptions{
		Topic:            config.C.PulsarInput.Topics.Events,
		SubscriptionName: config.C.PulsarInput.SubscriptionName,
		Type:             pulsar.Exclusive,
		MessageChannel:   msgChannel,
	}

	var consumer pulsar.Consumer = nil

	for i := 0; i < config.C.PulsarInput.MaxRetries; i++ {
		consumer, err = client.Subscribe(pulsarConfig)

		if err != nil {
			log.Println("Could not establish subscription. Retrying in " + config.C.PulsarInput.SleepTime.String())
			time.Sleep(config.C.PulsarInput.SleepTime)
		} else {
			break
		}
	}

	if consumer == nil {
		log.Fatalf("Could not establish subscription: %v", err)
	}

	defer consumer.Close()

	for cm := range msgChannel {
		msg := cm.Message

		// Process the message
		log.Println("New event:", msg.Key())
		log.Println("Payload:", msg.Payload())
		log.Println("Properties:", msg.Properties())

		var event custodian.Event
		err := json.Unmarshal(msg.Payload(), &event)
		if err != nil {
			log.Println("Error:", err.Error())
			continue // FIXME: Handle error?
		}

		if event.Token != "" {
			// Verify the token signature and extract the claims
			token, err := jwt.ParseWithClaims(event.Token, &custodian.TokenClaims{}, func(token *jwt.Token) (interface{}, error) {
				// get public key using kid
				var publicKey *ecdsa.PublicKey
				if kid, ok := token.Header["kid"].(string); !ok {
					return config.C.PDPPublicKey, nil // FIXME: handle differently?
				} else {
					if storedPk, ok := PublicKeys[kid]; ok {
						publicKey = storedPk
					} else {
						var err error
						publicKey, err = getPublicKey(ctx, kid)
						if err != nil {
							log.Println("Could not get public key:", err.Error())
							return config.C.PDPPublicKey, nil //FIXME
						}
						PublicKeys[kid] = publicKey
						log.Println("Public key:", publicKey)
					}
				}

				return publicKey, nil
			})
			if err != nil {
				log.Println(err.Error())
				//continue
			}

			if claims, ok := token.Claims.(*custodian.TokenClaims); ok { //FIXME:  should we add "&& token.Valid" ? We removed it because we do not want expired contract to have their logs masked
				// Add the claims to the event
				event.Attribute["actor_username"] = claims.Actor

				if userEvents, ok := Events[claims.UserID]; !ok {
					Events[claims.UserID] = []custodian.Event{event}
				} else {
					Events[claims.UserID] = append(userEvents, event)
				}
			} else {
				log.Println("Event not valid: could not get token claims")
				continue //FIXME: show error?
			}
		} else {
			log.Println("Event not valid: missing token")
			continue
		}

	}
}

// getPublicKey retreives a public key from the database
func getPublicKey(ctx context.Context, kid string) (*ecdsa.PublicKey, error) {
	var publicKeyData publicKeyStructure
	attributesMap := make(map[string]interface{})
	attributesMap["kid"] = kid

	err := MongoPK.GetAssetByFields(ctx, attributesMap, &publicKeyData, options.FindOne())
	if err != nil {
		return nil, err
	}

	publicKey, err := util.DecodeECDSAPublicKeyFromPEM([]byte(publicKeyData.PK))
	if err != nil {
		return nil, err
	}
	return publicKey, nil
}
