source .env

export KUBECONFIG=~/.kube/config
MANIFESTS_DIR=deploy/manifests/
NAMESPACE=custodian
KOMPOSE=$(go env GOPATH)/bin/kompose

kubectl delete -n $NAMESPACE -f $MANIFESTS_DIR

kubectl create namespace $NAMESPACE

dirs=(acs/docker-compose.yml audit-trail/docker-compose.yml cms/docker-compose.yml platform/docker-compose.yml demo/kc/docker-compose.yml kms/docker-compose.yml poc/mjf/sv-dc.yml)
for dc_file in ${dirs[@]};
do
	sudo docker compose -f $dc_file build
	echo $dc_file;
	resolved_dc=${dc_file::-4}-resolved.yml
	echo resolved file: $resolved_dc
	sudo docker compose -f $dc_file config > $resolved_dc
	#cat $resolved_dc
	$KOMPOSE convert -f $resolved_dc -o $MANIFESTS_DIR
done


for img in $(cat deploy/manifests/* | grep image | sed 's/.\+image: \+//g');
do
	#echo $img;
	sudo kind load docker-image $img
done

kubectl apply -n $NAMESPACE -f $MANIFESTS_DIR


echo Exposing ports...
kubectl -n $NAMESPACE get svc -o go-template='{{range .items}}{{range.spec.ports}}{{if .nodePort}}{{.port}}{{" "}}{{.nodePort}}{{" "}}{{end}}{{end}}{{end}}' > /tmp/ports_list_custodian
ports_list=($(cat /tmp/ports_list_custodian))
rm /tmp/ports_list_custodian

for (( i=0; i<${#ports_list[@]} ; i+= 2 ));
do
	port=${ports_list[i]}
	node_port=${ports_list[i+1]}
	echo $port:$node_port
	sudo docker stop custodian-kind-proxy-${port}
	sudo docker rm custodian-kind-proxy-${port}
	sudo docker run -d --name custodian-kind-proxy-${port} --publish 127.0.0.1:${port}:${port} --net kind --link kind-control-plane:target alpine/socat -dd tcp-listen:${port},fork,reuseaddr tcp-connect:target:${node_port}
done
