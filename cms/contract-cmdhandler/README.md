# Consent Command Handler

The command handler reads the consents message queue, executes the commands, and translates them in an event sourcing message queue. 


## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The consent commandhandler uses either Pulsar or Kafka as message queue. Make sure that the message queue you configured the code for (default is Pulsar) is currently running before running the code.

The same applies for MongoDB: the Mongo DB instance you configured your code for must be running.

### Build and Run

To build and run the code, open a terminal in the `custodian/cms/contract-cmdhandler` folder and run the following command:
```
go build -o contract-cmdhandler code/main.go
./contract-cmdhandler
```

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the `custodian/cms/contract-cmdhandler` folder or one of its subfolders, and run the following command:

```
godoc -http=localhost:6060
```

The documentation for the structures and interfaces is accessible at [http://localhost:6060/pkg/custodian/cms/contract-cmdhandler/code/go/](http://localhost:6060/pkg/custodian/cms/contract-cmdhandler/code/go/)
