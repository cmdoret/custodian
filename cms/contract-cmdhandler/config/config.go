/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import (
	"strings"
	"time"

	"github.com/spf13/viper"
)

type config struct {
	// Config struct for Input Pulsar Message Queue
	PulsarInput messageQueue `yaml:"PulsarInput"`

	// Config struct for Output Pulsar Message Queue
	PulsarOutput messageQueue `yaml:"PulsarOutput"`

	// Config struct for Kafka
	//KafkaInput messageQueue `yaml:"KafkaInput"`
	//KafkaOutput messageQueue `yaml:"KafkaOutput"`

	// Mongo DB struct
	MongoDB database `yaml:"MongoDB"`
}

type messageQueue struct {
	// Pulsar URL
	URL string `yaml:"url"`
	// Pulsar topics
	Topics topics `yaml:"topics"`
	// Messages keys
	Keys keys `yaml:"keys"`
	// Max number of retries to subscribe to channel
	MaxRetries int `yaml:"MaxRetries"`
	// Time to wait before each retry. Use a valid `time.ParseDuration` input (e.g., `5s` for 5 seconds, `150ms` for 150 milliseconds, etc.)
	SleepTime time.Duration `yaml:"SleepTime"`
	// Subscription name
	SubscriptionName string `yaml:"subscriptionName"`
}

type database struct {
	// Database URL
	URL string `yaml:"url"`
	// Database name
	Name string `yaml:"name"`
	// Tables of the database
	Tables tables `yaml:"tables"`
}

type topics struct {
	Contracts string `yaml:"contracts"`
	Status    string `yaml:"status"`
}

type keys struct {
	CreateContract string `yaml:"createContract"`
	UploadContract string `yaml:"uploadContract"`
	UpdateContract string `yaml:"updateContract"`
	DeleteContract string `yaml:"deleteContract"`
}

type tables struct {
	Dids      string `yaml:"dids"`
	Contracts string `yaml:"contracts"`
}

// We set a default values in case config.yml is not configured
// These default values will be replaced by values configured in config/config.yml
var C config /*= config{
	Server: server{"localhost","8004"},
	Pulsar: messageQueue{"pulsar://localhost:6650", topics{"contract_signing","contract_signing_errors"}, keys{"signing_contract","signing_contract"}},
	Kafka: messageQueue{"", topics{"",""}, keys{"",""}},
}*/

func ReadConfig() error {
	Config := &C
	viper.SetConfigName("config") // name of the config file
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if err := viper.Unmarshal(&Config); err != nil {
		return err
	}
	// remove after just for testing
	//spew.Dump(C)
	return nil
}
