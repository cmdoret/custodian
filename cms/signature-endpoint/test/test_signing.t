test signing:
  $ RESP=$(curl -X POST -s --show-error "http://$SIGNING_ENDPOINT/sign" -H  "accept: */*" -H  "Content-Type: application/json" -d "{\"user_oid\":\"abc\",\"consent_oid\":\"123\",\"signed_hash_b64\":\"XXX\",\"validity\":true}")
  $ echo $RESP
  ACK

