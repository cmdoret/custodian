# Signature Webservice

This REST API can be used to sign or revoke an existing contract.

The webservice receives requests and passes them in the messages queue.

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

The signature endpoint uses either Pulsar or Kafka as message queue. Make sure that the message queue you configured the code for (default is Pulsar) is currently running before running the code.

### Configuration

The config file can be found in `config/config.yml`. For the new configuration to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in the `custodian/cms/signature-endpoint` folder and run the following command:
```
go build -o signature-endpoint code/main.go
./signature-endpoint
```

## Usage

The list of endpoints and their description can be found in `api/openapi.yml`

## Documentation

To read the documentation you must install `godoc`. Open a terminal in the `custodian/cms/signature-endpoint` folder or one of its subfolders, and run the following command:

```
godoc -http=localhost:6060
```

The documentation for the structures and interfaces is accessible at [http://localhost:6060/pkg/custodian/cms/signature-endpoint/code/go/](http://localhost:6060/pkg/custodian/cms/signature-endpoint/code/go/)
