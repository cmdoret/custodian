/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	cms "custodian/cms/signature-endpoint/code/go"
	"custodian/cms/signature-endpoint/config"
	"log"
	"net/http"
)

func main() {
	log.Printf("Server started")

	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	// TODO: add SSL authentication (client AND server)

	router := cms.NewRouter()

	log.Printf(config.C.Server.Hostname + ":" + config.C.Server.Port)
	log.Fatal(http.ListenAndServe(config.C.Server.Hostname+":"+config.C.Server.Port, router))
}
