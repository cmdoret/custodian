/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package cms

import (
	"custodian/cms/signature-endpoint/config"
	mqconnector "gitlab.com/data-custodian/custodian-go/connectors/mq"

	"io"

	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"encoding/json"
)

var (
	mqProducer mqconnector.PulsarConnector
)

// extractRequestBody ready the body and returns its value in a byte array
func extractRequestBody(body io.Reader) ([]byte, error) {
	var requestBody interface{}

	err := json.NewDecoder(body).Decode(&requestBody)
	if err != nil {
		return nil, err
	}

	msg, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}

	return msg, nil
}

// genericQueryHandler passes the user's request to the message queue, and assigns a request ID to it.
func genericQueryHandler(w http.ResponseWriter, r *http.Request, topic string, key string) {
	mqProducer = mqconnector.PulsarConnector{
		URL: config.C.Pulsar.URL,
	}

	msg, err := extractRequestBody(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// FIXME: use a better msg ID system
	properties := make(map[string]string)
	requestID := fmt.Sprintf("%d%d", time.Now().UnixNano(), rand.Uint64())
	properties["requestID"] = requestID

	err = mqProducer.SendMessage(r.Context(), msg, key, topic, properties)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	} else {
		fmt.Fprintf(w, "%s", requestID)
	}
}

func SignContract(w http.ResponseWriter, r *http.Request) {
	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	genericQueryHandler(w, r, config.C.Pulsar.Topics.ContractSigning, config.C.Pulsar.Keys.Sign)
}

func RevokeContract(w http.ResponseWriter, r *http.Request) {
	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}
	genericQueryHandler(w, r, config.C.Pulsar.Topics.ContractSigning, config.C.Pulsar.Keys.Revoke)
}
