/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"crypto/ecdsa"
	"custodian/cms/signature-cmdhandler/config"

	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	mqconnector "gitlab.com/data-custodian/custodian-go/connectors/mq"
	custodian "gitlab.com/data-custodian/custodian-go/models"
	"gitlab.com/data-custodian/custodian-go/util"

	"bytes"
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type publicKey struct {
	UserID string `json:"userid" bson:"userid"`
	Kid    string `json:"kid" bson:"kid"`
	PK     string `json:"pk" bson:"pk"`
}

type revocationRequest struct {
	SignatureID string `json:"signature_id"`
	UserID      string `json:"userid"`
}

type revocation struct {
	ContractID   string   `json:"contract_id"`
	SignatureIDs []string `json:"signature_ids"`
}

var (
	mongoDid        mongofunc.MongoConnection
	mongoPK         mongofunc.MongoConnection
	mongoContracts  mongofunc.MongoConnection
	mongoSignatures mongofunc.MongoConnection

	// To use Pulsar
	mqProducer mqconnector.PulsarConnector

	mqConsumer mqconnector.PulsarConnector

	encodedPrivKey string

	current_id uint64 = 0 //FIXME: Store this somewhere else
)

// produceMessage sends a message to the "contract signature" topic
func produceMessage(ctx context.Context, message []byte, key string) {
	err := mqProducer.SendMessage(ctx, message, key, config.C.PulsarOutput.Topics.ContractSigning, nil)
	if err != nil {
		log.Fatal("Error: ", err.Error())
	}
}

// statusMessage sends a message to the "status" topic
func statusMessage(ctx context.Context, message string, key string, requestID string) {
	properties := make(map[string]string)
	properties["requestID"] = requestID
	err := mqProducer.SendMessage(ctx, []byte(message), key, config.C.PulsarOutput.Topics.Status, properties)
	if err != nil {
		log.Fatal("Error: ", err.Error())
	}
}

// getPublicKey retrieves a public key from the database using the user ID and the key ID in `signee`
//
// Currently, public keys are stored in a mongo database. This may change in the future.
func getPublicKey(ctx context.Context, signee string) (*ecdsa.PublicKey, error) {
	userID := strings.Split(signee, "?")[0]

	u, err := url.Parse(signee)
	if err != nil {
		return nil, err
	}
	if kid, ok := u.Query()["kid"]; !ok {
		return nil, errors.New("Missing KID")
	} else {
		var publicKey publicKey
		attributesMap := make(map[string]interface{})
		attributesMap["userid"] = userID
		attributesMap["kid"] = kid[0]

		fmt.Println(attributesMap)

		err := mongoPK.GetAssetByFields(ctx, attributesMap, &publicKey, options.FindOne())
		if err != nil {
			return nil, err
		}
		fmt.Println("PUBLIC KEY:", publicKey)
		return util.DecodeECDSAPublicKeyFromPEM([]byte(publicKey.PK))
	}
}

func verifyContractSignature(ctx context.Context, contractSignature custodian.Signature) (bool, error) {
	// Verify structure

	// Verify that the body matches the digest
	last := len(contractSignature.Seal) - 1

	if last < 0 {
		return false, errors.New("Signature Seal is empty")
	}

	valid, err := verifyContractBody(ctx, contractSignature.Header.Body.ID, contractSignature.Header.Body.Digest, contractSignature.Seal[last].Timestamp)
	if err != nil {
		log.Println(err)
		return false, err
	} else if !valid {
		log.Println("Contract body verification failed")
		return false, nil
	}

	publicKey, err := getPublicKey(ctx, contractSignature.Seal[last].Signee)
	if err != nil {
		log.Println("Public key not found")
		return false, errors.New("Public key not found")
	}

	valid, err = util.VerifyLastContractSignature(contractSignature, publicKey)
	if err != nil {
		log.Println(err)
		return false, err
	} else if !valid {
		log.Println("Last contract signature not valid")
		return false, nil
	} else {
		// If the signature is first in the chain (i.e., the oldest signature of the chain), then the chain is valid.
		if contractSignature.Header.Prev == (custodian.Link{}) {
			return true, nil
		} else {
			var prevContractSignature custodian.Signature

			err := mongoDid.GetAsset(ctx, contractSignature.Header.Prev.ID, &prevContractSignature)
			if err != nil {
				return false, errors.New("Prev contract signature not found")
			}

			// We check that the prev signature corresponds to the hash
			prevHash, err := prevContractSignature.Hash()
			if err != nil {
				return false, err
			} else if contractSignature.Header.Prev.Digest != b64.StdEncoding.EncodeToString(prevHash) {
				log.Println("Prev digest does not match")
				return false, nil
			}

			// We verify if the body changed between the two signatures
			if contractSignature.Header.Body.ID != prevContractSignature.Header.Body.ID {
				// Check that the new body respects the RegEx
				fmt.Println("ContractBody RegEx verification")
				valid, err := verifyContractTemplate(ctx, contractSignature.Header.Body.ID, prevContractSignature.Header.Body.ID)
				if err != nil {
					return false, err
				} else if !valid {
					log.Println("Body does not match contract template")
					return false, nil
				}
			}

			// Recursive call to verifyContractSignature
			return verifyContractSignature(ctx, prevContractSignature)
		}
	}

}

// verifyContractBody checks the validity of the contract, and verifies that its b64-encoded hash corresponds to `contractHashB64`
func verifyContractBody(ctx context.Context, contractOID string, contractHashB64 string, timestamp time.Time) (bool, error) {
	var contractString custodian.ContractString
	var contractBody custodian.ContractBody

	err := mongoDid.GetAsset(ctx, contractOID, &contractString)
	if err != nil {
		fmt.Println("Contract body not found")
		return false, errors.New("Contract body not found")
	}

	fmt.Println(contractString)

	err = json.Unmarshal([]byte(contractString.Contract), &contractBody)
	if err != nil {
		log.Println(err.Error())
		return false, err
	}

	if timestamp.Before(contractBody.Validity.NotBefore) || timestamp.After(contractBody.Validity.NotAfter) {
		log.Println("Timestamp not in time range")
		return false, errors.New("Timestamp not in time range")
	}

	contractHash := contractString.Hash()

	if b64.StdEncoding.EncodeToString(contractHash) != contractHashB64 {
		log.Println(b64.StdEncoding.EncodeToString(contractHash), "!=", contractHashB64)
		return false, errors.New("Body digest does not match")
	}

	return true, nil
}

// verifyContractTemplate checks if the contract with ID `contractOID` respects the contract template with ID `templateOID`
func verifyContractTemplate(ctx context.Context, contractOID string, templateOID string) (bool, error) {
	var contractString custodian.ContractString
	var templateString custodian.ContractString

	var contractBody custodian.ContractBody
	var templateBody custodian.ContractBody

	err := mongoDid.GetAsset(ctx, contractOID, &contractString)
	if err != nil {
		fmt.Println("Contract body not found")
		return false, errors.New("Contract body not found")
	}
	err = mongoDid.GetAsset(ctx, templateOID, &templateString)
	if err != nil {
		fmt.Println("Contract template not found")
		return false, errors.New("Contract template not found")
	}

	err = json.Unmarshal([]byte(contractString.Contract), &contractBody)
	if err != nil {
		fmt.Println(err.Error())
		return false, err
	}

	err = json.Unmarshal([]byte(templateString.Contract), &templateBody)
	if err != nil {
		fmt.Println(err.Error())
		return false, err
	}

	sort.Slice(contractBody.Var, func(i, j int) bool { return contractBody.Var[i].ID < contractBody.Var[j].ID }) // Sort vars by ID to make sure they are in the same order
	sort.Slice(templateBody.Var, func(i, j int) bool { return templateBody.Var[i].ID < templateBody.Var[j].ID }) // Sort vars by ID to make sure they are in the same order

	fmt.Println(contractBody)
	fmt.Println(templateBody)

	// FIXME: We only look at the variables here, we should look other fields too

	if len(contractBody.Var) != len(templateBody.Var) {
		log.Println("Template and contract do not have the same number of variables")
		return false, errors.New("Template and contract do not have the same number of variables")
	}

	for i := 0; i < len(contractBody.Var); i++ {
		if templateBody.Var[i].Regexp != "" {
			re := regexp.MustCompile(templateBody.Var[i].Regexp)
			re.Longest()
			if contractBody.Var[i].Value != re.FindString(contractBody.Var[i].Value) {
				log.Println(contractBody.Var[i].Value, "does not match regexp:", templateBody.Var[i].Regexp)
				return false, nil
			}
		}
	}

	// FIXME: look that every other fields did not change (except tags)

	return true, nil
}

// SignContract adds a new contract signature to the database.
//
// The function validates the new signature, adds the CMS seal to it (in a new signature struct), and adds both structs to the database and message queue.
func SignContract(ctx context.Context, msg []byte, requestID string) {
	err := config.ReadConfig() // We reload the config file in case the CMS private key has changed
	if err != nil {
		log.Printf(err.Error())
		return
	}

	var contractSignature custodian.Signature

	err = json.Unmarshal(msg, &contractSignature)

	fmt.Println("Contract Signature:", contractSignature)
	if err != nil {
		log.Println(err.Error())
		return
	}

	exists, err := mongoDid.Exists(ctx, contractSignature.OID)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	} else if exists {
		log.Println("OID already exists")
		statusMessage(ctx, "Error: OID already exists", config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	// We verify the signatures
	valid, err := verifyContractSignature(ctx, contractSignature)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}
	if !valid {
		log.Println("Contract signature not valid.")
		statusMessage(ctx, "Error: Contract signature not valid.", config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	// We store the user's signature in DIDs database
	err = mongoDid.AddAsset(ctx, contractSignature.OID, &contractSignature)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	// Also store signature in CMS's view
	err = mongoSignatures.AddAsset(ctx, contractSignature.OID, &contractSignature)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	msg, err = json.Marshal(contractSignature)
	if err != nil {
		fmt.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	produceMessage(ctx, msg, config.C.PulsarOutput.Keys.Sign)

	// Add CMS seal

	newContractSignature, err := util.SignSignature(contractSignature, config.C.PrivateKey, config.C.CmsOID, config.C.CmsSource)
	if err != nil {
		log.Println(err)
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	// Store the CMS signature in DIDs database
	err = mongoDid.AddAsset(ctx, newContractSignature.OID, &newContractSignature)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	// Also add signature in CMS's view
	err = mongoSignatures.AddAsset(ctx, newContractSignature.OID, &newContractSignature)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	msg, err = json.Marshal(newContractSignature)
	if err != nil {
		log.Println(err.Error())
		statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	produceMessage(ctx, msg, config.C.PulsarOutput.Keys.Sign)

	fmt.Println("OK signed contract")

	statusMessage(ctx, "Contract signed.", config.C.PulsarOutput.Keys.Sign, requestID)

	// FIXME
	last := len(contractSignature.Seal) - 1

	if last < 0 {
		statusMessage(ctx, "Error: signature seal is empty", config.C.PulsarOutput.Keys.Sign, requestID)
		return
	}

	userID := strings.Split(contractSignature.Seal[last].Signee, "?")[0]
	fmt.Println(userID)

	claims := custodian.TokenClaims{
		UserID:      userID,
		ClientID:    "cms", //FIXME: hardcoded key, we must make sure it is there
		SignatureID: contractSignature.OID,
		Actor:       config.C.CmsOID,
		Scope:       nil, // FIXME: list of ScopeStruct.Verb
		Constraints: nil,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
			Issuer:    config.C.CmsOID,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["kid"] = config.C.Kid // FIXME: change config key name
	signedToken, err := token.SignedString(config.C.PrivateKey)

	err = sendEvent(time.Now().Unix(), signedToken, "contract-signature", "sign", map[string]interface{}{"signature_id": contractSignature.OID})
	if err != nil {
		fmt.Println(err)
		return
	}
}

// RevokeContract blacklists contracts and signatures in the DB.
//
//	The signature sent to the endpoint must be the signature of the contract template. All the signatures and contract created from this template for the user specified in paramter are removed.
//	msg contains: signature of the contract template + user ID
func RevokeContract(ctx context.Context, msg []byte, requestID string) {
	err := config.ReadConfig() // We reload the config file in case the CMS private key has changed
	if err != nil {
		log.Printf(err.Error())
		return
	}

	var revocationRequest revocationRequest

	err = json.Unmarshal(msg, &revocationRequest)
	if err != nil {
		log.Println(err.Error())
		return
	}
	fmt.Println(revocationRequest)

	if revocationRequest.UserID == "" || revocationRequest.SignatureID == "" {
		log.Println("Invalid revocation request:", revocationRequest)
		return
	}

	// Retreive signature

	var contractSignature custodian.Signature

	err = mongoSignatures.GetAsset(ctx, revocationRequest.SignatureID, &contractSignature)
	if err != nil {
		log.Println("Signature not found")
		return
	}
	fmt.Println("Template signature:", contractSignature)
	// Retreive the contract template attached to the signature

	var contractBody custodian.ContractBody

	err = mongoContracts.GetAsset(ctx, contractSignature.Header.Body.ID, &contractBody)
	if err != nil {
		log.Println("Contract not found")
		return
	}

	tags := append(contractBody.Tag, custodian.Tag{"userid", revocationRequest.UserID})

	fmt.Println("New tags:", tags)

	tagsList := []bson.M{}

	for _, tag := range tags {
		tagsList = append(tagsList, bson.M{"tag.name": tag.Name, "tag.value": tag.Value})
	}

	filter := bson.M{"$and": tagsList}

	fmt.Println("Filter", filter)

	cursor, err := mongoContracts.GetAssetsByFilterSorted(ctx, filter, "_id", -1)
	if err != nil {
		log.Println(err)
		return
	}

	var contracts []custodian.ContractBody
	if err = cursor.All(ctx, &contracts); err != nil {
		log.Println(err)
		return
	}

	fmt.Println("Contracts found", contracts)

	for _, contract := range contracts {
		cursor, err := mongoSignatures.GetAssetsByField(ctx, "header.body.id", contract.OID)
		if err != nil {
			log.Println(err)
			continue
		}
		var signatures []custodian.Signature
		if err = cursor.All(ctx, &signatures); err != nil {
			log.Println(err)
			continue
		}

		var signatureIDs []string
		for _, signature := range signatures {
			signatureIDs = append(signatureIDs, signature.OID)
		}

		revocation := revocation{contract.OID, signatureIDs}
		// Revoke
		msg, err = json.Marshal(revocation)
		if err != nil {
			log.Println(err.Error())
			statusMessage(ctx, "Error: "+err.Error(), config.C.PulsarOutput.Keys.Revoke, requestID)
			continue
		}

		produceMessage(ctx, msg, config.C.PulsarOutput.Keys.Revoke)

		// Remove the contract from DB
		err = mongoContracts.DeleteAsset(ctx, revocation.ContractID)
		if err != nil {
			log.Println(err)
			continue
		}

		for _, signatureID := range revocation.SignatureIDs {
			// Remove the signature from DB
			err = mongoSignatures.DeleteAsset(ctx, signatureID)
			if err != nil {
				log.Println(err)
				continue
			}
		}

	}

	// Create event log for the revocation
	signedToken, err := createToken(revocationRequest.UserID, revocationRequest.SignatureID)
	if err != nil {
		log.Println(err)
		return
	}

	err = sendEvent(time.Now().Unix(), signedToken, "contract-signature", "revoke", map[string]interface{}{"signature_id": revocationRequest.SignatureID})
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println("OK revoked consent")

	statusMessage(ctx, "Consent revoked.", config.C.PulsarOutput.Keys.Revoke, requestID)

}

// createToken returns a custom JWT filled with the user and signature IDs
func createToken(userID string, signatureID string) (string, error) {
	claims := custodian.TokenClaims{
		UserID:      userID,
		ClientID:    "cms", //FIXME: hardcoded key, we must make sure it is there
		SignatureID: signatureID,
		Actor:       config.C.CmsOID,
		Scope:       nil, // FIXME: list of ScopeStruct.Verb
		Constraints: nil,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
			Issuer:    config.C.CmsOID,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["kid"] = config.C.Kid // FIXME: change config key name
	signedToken, err := token.SignedString(config.C.PrivateKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

// FIXME: DRY (also in PDP)
// sendEvent sends a new event to the EPP endpoint
func sendEvent(timestamp int64, token string, resource string, action string, attribute map[string]interface{}) error {
	event := custodian.Event{
		Unix:      timestamp,
		Token:     token,
		ID:        fmt.Sprintf("%d", current_id),
		Resource:  resource,
		Action:    action,
		Attribute: attribute,
	}

	jsonEvent, err := json.Marshal(event)
	if err != nil {
		log.Println("Error:", err)
		return err
	}
	current_id += 1

	resp, err := http.Post(config.C.AuditTrailUrl+"/event", "application/json", bytes.NewBuffer(jsonEvent))
	if err != nil {
		return err
	}

	fmt.Printf("EPP Response: %d", resp.StatusCode)
	return nil
}

func main() {
	ctx := context.Background()

	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	mongoDid = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Dids,
	)

	mongoPK = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.PublicKeys,
	)

	mongoContracts = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Contracts,
	)

	mongoSignatures = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Signatures,
	)

	// Close the clients connections
	defer mongoDid.CloseClientDB(ctx)
	defer mongoPK.CloseClientDB(ctx)
	defer mongoContracts.CloseClientDB(ctx)
	defer mongoSignatures.CloseClientDB(ctx)

	// To use Pulsar
	mqProducer = mqconnector.PulsarConnector{
		URL: config.C.PulsarOutput.URL,
	}

	/*
		mqConsumer = mqconnector.PulsarConnector{
			URL:              config.C.PulsarInput.URL,
			ConsumeAndDelete: true,
		} */

	// To use Kafka
	/*
			mqProducer = mqconnector.KafkaConnector{
			    config.C.KafkaInput.URL,
			    10*time.Second,
			}

		    mqConsumer = mqconnector.KafkaConnector{
		        config.C.KafkaOutput.URL,
		        10*time.Second,
		    }
	*/
	/*
		// Maps the functions to the commands keys
		commands := make(map[string]mqconnector.CommandHandler)

		commands[config.C.PulsarInput.Keys.Sign] = SignContract

		mqConsumer.SubscribeAndConsume(ctx, config.C.PulsarInput.Topics.ContractSigning, "my-subscription-1", commands, config.C.PulsarInput.MaxRetries, config.C.PulsarInput.SleepTime)
	*/

	msgChannel := make(chan pulsar.ConsumerMessage)

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: config.C.PulsarInput.URL,
	})

	if err != nil {
		log.Fatalf("Could not create client: %v", err)
	}

	pulsarConfig := pulsar.ConsumerOptions{
		Topic:            config.C.PulsarInput.Topics.ContractSigning,
		SubscriptionName: config.C.PulsarInput.SubscriptionName,
		Type:             pulsar.Exclusive,
		MessageChannel:   msgChannel,
	}

	var consumer pulsar.Consumer = nil

	for i := 0; i < config.C.PulsarInput.MaxRetries; i++ {
		consumer, err = client.Subscribe(pulsarConfig)

		if err != nil {
			log.Println("Could not establish subscription. Retrying in " + config.C.PulsarInput.SleepTime.String())
			time.Sleep(config.C.PulsarInput.SleepTime)
		} else {
			break
		}
	}

	if consumer == nil {
		log.Fatalf("Could not establish subscription: %v", err)
	}

	defer consumer.Close()

	for cm := range msgChannel {
		msg := cm.Message

		// Process the message
		fmt.Println(msg.Key(), msg.Payload(), msg.Properties())

		// Save file in destination folder

		requestID, ok := msg.Properties()["requestID"]
		if !ok {
			fmt.Println("No message ID")
		} else {
			switch msg.Key() {
			case config.C.PulsarInput.Keys.Sign:
				SignContract(ctx, msg.Payload(), requestID)
			case config.C.PulsarInput.Keys.Revoke:
				RevokeContract(ctx, msg.Payload(), requestID)
			default:
				fmt.Println("Unrecognized Key:", msg.Key())
			}

		}

		// To remove the message from the MQ
		consumer.Ack(msg) // FIXME: do we want to keep the messages?

	}

}
