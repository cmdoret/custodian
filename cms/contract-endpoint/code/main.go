/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"custodian/cms/contract-endpoint/code/router"
	"custodian/cms/contract-endpoint/config"
	"fmt"
	"log"
	"net/http"
)

func main() {
	// read yaml file configuration
	err := config.ReadConfig()
	if err != nil {
		log.Printf(err.Error())
		return
	}

	// instantiate the Router
	router := router.Router()

	// TODO: add SSL authentication (client AND server)

	fmt.Println("Server listen at http://localhost" + ":" + config.C.Server.Port)
	log.Fatal(http.ListenAndServe(config.C.Server.Hostname+":"+config.C.Server.Port, router))

}
