package controller

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestExtractRequestBody(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "test")
	}))
	defer testServer.Close()

	resp, err := http.Get(testServer.URL)
	if err != nil {
		t.Fatal(err)
	}

	requestBody, err := extractRequestBody(resp.Body)
	if err != nil {
		t.Fatal(err)
	} else if string(requestBody) != "test" {
		t.Fatal("extractRequestBody did not return the correct value")
	}
}
