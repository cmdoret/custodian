/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package controller

import (
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"custodian/cms/did-resolver/config"
	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
)

var (
	mongoDid mongofunc.MongoConnection // Setup in main
)

// GetDocument writes the document (JSON) that corresponds to the URL query parameter `id` to the response writer
func GetDocument(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	id_b64, ok := r.URL.Query()["id"]
	if !ok {
		http.Error(w, "Missing ID", http.StatusBadRequest)
		return
	}

	mongoDid = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,
		config.C.MongoDB.Name,
		config.C.MongoDB.Tables.Dids,
	)

	// Close the clients connections
	defer mongoDid.CloseClientDB(ctx)

	b64_string := id_b64[0]

	// Padding for base64. The b64 string length must be a multiple of 4. Otherwise, we repair it.
	b64_padding_length := (4 - (len(b64_string) % 4)) % 4
	if b64_padding_length > 2 {
		b64_string = b64_string[:len(b64_string)-1]
	} else {
		b64_string = b64_string + strings.Repeat("=", b64_padding_length)
	}

	log.Println("New query:", b64_string)

	id, err := b64.URLEncoding.DecodeString(b64_string)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	var did map[string]interface{}
	err = mongoDid.GetAsset(ctx, string(id), &did)
	if err != nil {
		http.Error(w, "ID not found", http.StatusNotFound)
		return
	}

	log.Println("Response:", did)

	// If the document is a `ContractString`, we directly return its value (JSON)
	if contract, ok := did["contract"]; ok {
		fmt.Fprintf(w, "%s", contract)
	} else {
		delete(did, "_id")
		jsonString, err := json.Marshal(did)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "%s", string(jsonString))
	}

	return
}
