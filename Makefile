pre-commit-hook:
	@echo "\e[1;34m Copying pre-commit hook to .git/hook folder...\e[0m"
	@cp .git-commit-hooks/pre-commit .git/hooks/pre-commit
	@echo "\e[1;34m Done.\e[0m"
