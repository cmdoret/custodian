#!/bin/bash

if [ "$#" -ne 3 ] || ! [[ $1 =~ ^[a-zA-Z-]+$ ]] || ! [[ $2 =~ ^[a-zA-Z-]+$ ]] ; then
  echo "Usage: $0 COMMON_NAME POD_NAME folder" >&2
  exit 1
fi

echo $# $1 $2

mkdir -p $3
CERTS_PATH=$3
SERVER_KEY="$CERTS_PATH/$1.key"
SERVER_CSR="$CERTS_PATH/$1.csr"
SERVER_CRT="$CERTS_PATH/$1.crt"
EXTFILE_SAMPLE="cert_ext.cnf.sample"
EXTFILE="cert_ext.cnf"
OPENSSL_CMD="/usr/bin/openssl"
COMMON_NAME="$1"
POD_NAME="$2"

CA_KEY="./secrets/ca/custodianCA.key"
CA_CRT="./secrets/ca/custodianCA.crt"
CA_EXT="./secrets/ca/custodianCA.cnf"
CA_EXT_SAMPLE="./custodianCA.cnf.sample"

# generating server key
echo "Generating private key"
$OPENSSL_CMD genrsa -out $SERVER_KEY 4096 2>/dev/null
if [ $? -ne 0 ] ; then
   echo "ERROR: Failed to generate $SERVER_KEY"
   exit 1
fi

## Update Common Name in External File
cp $EXTFILE_SAMPLE $EXTFILE
/bin/echo "commonName              = $COMMON_NAME" >> $EXTFILE

cp $CA_EXT_SAMPLE $CA_EXT
/bin/echo "DNS.2 = $POD_NAME" >> $CA_EXT

# Generating Certificate Signing Request using config file
echo "Generating Certificate Signing Request"
$OPENSSL_CMD req -new -key $SERVER_KEY -out $SERVER_CSR -config $EXTFILE 2>/dev/null
if [ $? -ne 0 ] ; then
   echo "ERROR: Failed to generate $SERVER_CSR"
   exit 1
fi


#echo "Generating self signed certificate"
#$OPENSSL_CMD x509 -req -days 3650 -in $SERVER_CSR -signkey $SERVER_KEY -out $SERVER_CRT 2>/dev/null
#if [ $? -ne 0 ] ; then
#   echo "ERROR: Failed to generate self-signed certificate file $SERVER_CRT"
#fi

echo "Generating RootCA signed server certificate"
$OPENSSL_CMD x509 -req -in $SERVER_CSR -CA $CA_CRT -CAkey $CA_KEY -out $SERVER_CRT -CAcreateserial -days 365 -sha512 -extfile $CA_EXT --passin file:secrets/ca/.pem_password
if [ $? -ne 0 ] ; then
    echo "ERROR: Failed to generate $SERVER_CRT"
    exit 1
fi
