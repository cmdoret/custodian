#!/bin/bash

PEM_PASSWORD_FILE=secrets/ca/.pem_password

mkdir -p secrets/ca
openssl rand -base64 24 > $PEM_PASSWORD_FILE

# Generate CA
openssl genrsa -des3 -out secrets/ca/custodianCA.key -passout file:$PEM_PASSWORD_FILE 4096
openssl req -new -x509 -days 3650 -config custodianCA_cert.cnf -key secrets/ca/custodianCA.key -out secrets/ca/custodianCA.crt -passin file:$PEM_PASSWORD_FILE

# Generate certs
bash create_ssl_certificate.sh custodian-acs-pdp accesscontrol-pdp secrets/acs/pdp/ssl
bash create_ssl_certificate.sh custodian-acs-pap accesscontrol-pap secrets/acs/pap/ssl
bash create_ssl_certificate.sh custodian-cms-contract cme-contract-endpoint secrets/cms/contract/ssl
bash create_ssl_certificate.sh custodian-cms-signature cme-signature-endpoint secrets/cms/signature/ssl
