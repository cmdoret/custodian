package main

import (
	"fmt"
	"math/rand"
	"os"

	"gopkg.in/yaml.v2"
)

type secretsStore struct {
	Secrets secrets `yaml:"secrets"`
}

type sslSecrets struct {
	Cert string `yaml:"cert"`
	Key  string `yaml:"key"`
}

type ecdsaSecrets struct {
	Public  string `yaml:"public"`
	Private string `yaml:"private"`
}

type secrets struct {
	Acs         acsSecrets `yaml:"acs"`
	Cms         cmsSecrets `yaml:"cms"`
	CustodianCA string     `yaml:"custodianCA"`
}

type keycloak struct {
	Auth       keycloakAuth `yaml:"auth"`
	Postgresql postgresql   `yaml:"postgresql"`
}

type keycloakAuth struct {
	AdminPassword string `yaml:"adminPassword"`
}

type postgresql struct {
	Auth postgresqlAuth `yaml:"auth"`
}

type postgresqlAuth struct {
	Password        string `yaml:"password"`
	PostgresPasswor string `yaml:"postgresPassword"`
}

type acsSecrets struct {
	PDP serviceSecrets `yaml:"pdp"`
}

type cmsSecrets struct {
	Signature serviceSecrets `yaml:"signature"`
}

type serviceSecrets struct {
	Ssl   sslSecrets   `yaml:"ssl"`
	Ecdsa ecdsaSecrets `yaml:"ecdsa"`
}

const (
	passwordCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=[]{}\\|;':\",.<>/?`~"
	passwordLength     = 20
)

func ReadFile(filename string) string {
	data, err := os.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	return string(data)
}

func generatePassword() string {
	b := make([]byte, passwordLength)
	for i := range b {
		b[i] = passwordCharacters[rand.Intn(len(passwordCharacters))]
	}
	return string(b)
}

func main() {

	keycloakConfig := keycloak{
		Auth: keycloakAuth{
			AdminPassword: generatePassword(),
		},
		Postgresql: postgresql{
			Auth: postgresqlAuth{
				Password:        generatePassword(),
				PostgresPasswor: generatePassword(),
			},
		},
	}

	secretsStore := secretsStore{
		Secrets: secrets{
			Acs: acsSecrets{
				PDP: serviceSecrets{
					Ssl: sslSecrets{
						Cert: ReadFile("secrets/acs/pdp/ssl/custodian-acs-pdp.crt"),
						Key:  ReadFile("secrets/acs/pdp/ssl/custodian-acs-pdp.key"),
					},
					Ecdsa: ecdsaSecrets{
						Public:  ReadFile("secrets/acs/pdp/ecdsa/public.pem"),
						Private: ReadFile("secrets/acs/pdp/ecdsa/private.pem"),
					},
				},
			},
			Cms: cmsSecrets{
				Signature: serviceSecrets{
					Ssl: sslSecrets{
						Cert: ReadFile("secrets/cms/signature/ssl/custodian-cms-signature.crt"),
						Key:  ReadFile("secrets/cms/signature/ssl/custodian-cms-signature.key"),
					},
					Ecdsa: ecdsaSecrets{
						Public:  ReadFile("secrets/cms/signature/ecdsa/public.pem"),
						Private: ReadFile("secrets/cms/signature/ecdsa/private.pem"),
					},
				},
			},
			CustodianCA: ReadFile("secrets/ca/custodianCA.crt"),
		},
	}

	newFile := make(map[string]interface{})
	newFile["global"] = secretsStore // We need the secrets to be set as global values in case we deploy only a few services of the Custodian
	newFile["custodian-keycloak"] = keycloakConfig

	yamlData, err := yaml.Marshal(&newFile)
	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}

	fmt.Println()
	fmt.Println("########################################################################")
	fmt.Println("# Use the following YAML file to configure your dev Custodian instance #")
	fmt.Println("########################################################################")
	fmt.Println()

	fmt.Println("# This file was generated using the secrets generator provided by the Swiss Data Custodian")
	fmt.Println(string(yamlData))
}
