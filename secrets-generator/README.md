# Secrets

This directory proposes various scripts for the generation of secrets used by the Custodian.

Here you can find a Dockerfile and tools to generate a YAML file that contains all the necessary secrets to deploy a local instance of the Custodian. This is meant for a development or test environment only. For production, please manually generate and configure the secrets.

## Run with Docker

Build the docker image

```sh
docker build -t secrets-generator .
```

Run the container an mount the current folder to store the YAML file in it.

```sh
docker run -it -u $UID -v "${PWD}:/work" secrets-generator
```

Fill in the form required to generate the SSL certificates.

A file `custodian-secrets.yaml` will appear in the folder. This file can be user to deploy a Custodian instance with Helm.

## Generate the files

If you do not need the YAML file and/or want to access the files that contain the secrets, you can use the bash scripts directly.

First, run the `scripts/configure_ca.sh` script to create the CA certificate config files, and then run `setup.sh` to create all the files.

```sh
./scripts/configure_ca.sh
# fill in the prompted form
# ...
./setup.sh
```

All the secrets are generated in a new `secrets` folder. The secrets folder is organized into folders and subfolders with respect to the different services/microservices/use-cases of the Custodian.
