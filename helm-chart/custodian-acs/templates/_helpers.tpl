{{/*
Expand the name of the chart.
*/}}
{{- define "custodian-acs.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "custodian-acs.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "custodian-acs.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "custodian-acs.labels" -}}
helm.sh/chart: {{ include "custodian-acs.chart" . }}
{{ include "custodian-acs.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "custodian-acs.selectorLabels" -}}
app.kubernetes.io/name: {{ include "custodian-acs.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "custodian-acs.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "custodian-acs.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the pulsar URL
*/}}
{{- define "custodian.pulsarUrl" -}}
{{- if .Values.pulsar.url }}
{{- .Values.pulsar.url }}
{{- else -}}
pulsar://{{ .Release.Name }}-custodian-pulsar-broker:6650
{{- end }}
{{- end }}

{{/*
Create the mongodb URL
*/}}
{{- define "custodian.mongodbUrl" -}}
{{- if .Values.mongodb.url }}
{{- .Values.mongodb.url }}
{{- else -}}
mongodb://{{ .Release.Name }}-custodian-mongodb:27017
{{- end }}
{{- end }}