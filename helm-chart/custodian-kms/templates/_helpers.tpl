{{/*
Create the mongodb URL
*/}}
{{- define "custodian.mongodbUrl" -}}
{{- if .Values.mongodb.url }}
{{- .Values.mongodb.url }}
{{- else -}}
mongodb://{{ .Release.Name }}-custodian-mongodb:27017
{{- end }}
{{- end }}