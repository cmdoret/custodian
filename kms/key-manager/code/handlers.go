/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"context"
	"custodian/kms/key-manager/config"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/sirupsen/logrus"
	mongofunc "gitlab.com/data-custodian/custodian-go/connectors/mongo"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type publicKey struct {
	Id     string `json:"id,omitempty"` // Only for mongoDB
	Kid    string `json:"kid"`
	UserId string `json:"userid,omitempty"`
	Pk     string `json:"pk"`
}

type claims struct {
	Email    string `json:"email"`
	Verified bool   `json:"email_verified"`
	Sub      string `json:"sub"`
}

const (
	oidcPrefixBearer string = "Bearer "
)

var (
	mongoPublicKeys mongofunc.MongoConnection
)

// FIXME: Hack
func (p publicKey) GetOID() string {
	return fmt.Sprintf("%v-%v", p.UserId, p.Kid)
}

func (p publicKey) SetOID(OID string) {
	p.Id = OID
	return
}
func (p publicKey) String() string {
	return fmt.Sprintf("{kid: %v, userid: %v, pk: %v}", p.Kid, p.UserId, p.Pk)
}

func extractIDToken(h http.Header) string {
	auth := h.Get("Authorization")
	if auth == "" {
		return ""
	}
	if !strings.HasPrefix(auth, oidcPrefixBearer) {
		return ""
	}

	return strings.TrimPrefix(auth, oidcPrefixBearer)
}

func extractUserClaims(ctx context.Context, rawIDToken string) (claims, error) {
	// TODO: move this somewhere else
	provider, err := oidc.NewProvider(ctx, config.C.OIDC.Issuer)
	if err != nil {
		logrus.Errorf("Failed to create provider: %v", err)
		return claims{}, err
	}

	verifier := provider.Verifier(&oidc.Config{ClientID: config.C.OIDC.ClientID})

	// Parse and verify ID Token payload.
	idToken, err := verifier.Verify(ctx, rawIDToken)
	if err != nil {
		// handle error
		logrus.Errorf("Failed to verify ID Token: %v", err)
		return claims{}, err
	}

	logrus.Infof("Verified ID Token: %v", idToken)

	// Extract custom claims
	var userClaims claims

	if err := idToken.Claims(&userClaims); err != nil {
		// handle error
		logrus.Errorf("Failed to extract claims: %v", err)
		return claims{}, err
	}

	return userClaims, nil
}

// GetPublicKey returns the public key with the given userid and kid.
func GetPublicKey(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	// Get the kid from the query parameters
	kid := r.URL.Query().Get("kid")
	if kid == "" {
		http.Error(w, "Missing kid", http.StatusBadRequest)
		return
	}
	userid := r.URL.Query().Get("userid")
	if userid == "" {
		http.Error(w, "Missing userid", http.StatusBadRequest)
		return
	}

	mongoPublicKeys = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,               // MongoDB URL
		config.C.MongoDB.Name,              // Database name
		config.C.MongoDB.Tables.PublicKeys, // Table in the database
	)

	// Close the DB connections when the program exits
	defer mongoPublicKeys.CloseClientDB(ctx)

	// Create a map of attributes to search for
	attributesMap := map[string]interface{}{
		"kid":    kid,
		"userid": userid,
	}

	var publicKey publicKey

	// Get the public key from the database
	err := mongoPublicKeys.GetAssetByFields(ctx, attributesMap, &publicKey, options.FindOne())
	if err != nil {
		switch err {
		case mongo.ErrNoDocuments:
			logrus.Errorf("Public key not found: %v", err)
			http.Error(w, "Public key not found", http.StatusNotFound)
		default:
			logrus.Errorf("Failed to get public key: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	// Marshal the public key to json
	jsonPublicKey, err := json.Marshal(publicKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write the json to the response
	w.Write(jsonPublicKey)
}

func GetMyPublicKeys(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	rawIDToken := extractIDToken(r.Header)
	if rawIDToken == "" {
		http.Error(w, "Access denied", http.StatusUnauthorized)
		return
	}

	// Extract the user ID from the ID token
	claims, err := extractUserClaims(ctx, rawIDToken)
	if err != nil {
		logrus.Errorf("Failed to extract user ID: %v", err)
		http.Error(w, "ID token not valid", http.StatusUnauthorized)
		return
	}

	mongoPublicKeys = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,               // MongoDB URL
		config.C.MongoDB.Name,              // Database name
		config.C.MongoDB.Tables.PublicKeys, // Table in the database
	)

	// Close the DB connections when the program exits
	defer mongoPublicKeys.CloseClientDB(ctx)

	// Get all the public keys from the database
	cursor, err := mongoPublicKeys.GetAssetsByField(ctx, "userid", claims.Sub)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	publicKeys := []publicKey{}
	if err = cursor.All(ctx, &publicKeys); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Marshal the filtered public keys to json
	jsonPublicKeys, err := json.Marshal(publicKeys)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write the json to the response
	w.Write(jsonPublicKeys)

	return
}
func PostPublicKey(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	rawIDToken := extractIDToken(r.Header)
	if rawIDToken == "" {
		http.Error(w, "Access denied", http.StatusUnauthorized)
		return
	}

	// Extract the user ID from the ID token
	claims, err := extractUserClaims(ctx, rawIDToken)
	if err != nil {
		logrus.Errorf("Failed to extract user ID: %v", err)
		http.Error(w, "ID token not valid", http.StatusUnauthorized)
		return
	}

	mongoPublicKeys = mongofunc.NewMongoDB(
		config.C.MongoDB.URL,               // MongoDB URL
		config.C.MongoDB.Name,              // Database name
		config.C.MongoDB.Tables.PublicKeys, // Table in the database
	)

	// Close the DB connections when the program exits
	defer mongoPublicKeys.CloseClientDB(ctx)

	// Verify if the key already exists

	var publicKey publicKey
	err = json.NewDecoder(r.Body).Decode(&publicKey)
	if err != nil {
		http.Error(w, "Invalid query", http.StatusBadRequest)
		return
	}

	// Add the user ID to the public key
	publicKey.UserId = claims.Sub

	log.Println("New public key received:", publicKey)

	// Verify if the public key already exists
	if exists, err := mongoPublicKeys.Exists(ctx, publicKey.GetOID()); exists {
		http.Error(w, "Public key already exists", http.StatusConflict)
		return
	} else if err != nil {
		log.Println(err.Error())
		return
	}

	err = mongoPublicKeys.AddAsset(ctx, publicKey.GetOID(), publicKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	return
}
