package main

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type KeySet struct{}

const idToken = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImFhYSI6NjkyfQ.eyJhY3IiOiJzb21ldGhpbmciLCJhbXIiOlsiZm9vIiwiYmFyIl0sImF0X2hhc2giOiIyZHpibV92SXh5LTdlUnRxVUlHUFB3IiwiYXVkIjpbInVuaXQiLCJ0ZXN0IiwiNTU1NjY2Il0sImF1dGhfdGltZSI6MTY3ODEwMDk2MSwiYXpwIjoiNTU1NjY2IiwiYmFyIjp7ImNvdW50IjoyMiwidGFncyI6WyJzb21lIiwidGFncyJdfSwiY2xpZW50X2lkIjoiNTU1NjY2IiwiZXhwIjo0ODAyMjM4NjgyLCJmb28iOiJIZWxsbywgV29ybGQhIiwiaWF0IjoxNjc4MTAxMDIxLCJpc3MiOiJsb2NhbGhvc3Q6NDQ0NCIsImp0aSI6Ijk4NzYiLCJuYmYiOjE2NzgxMDEwMjEsIm5vbmNlIjoiMTIzNDUiLCJzdWIiOiJ0aW1AbG9jYWwuY29tIn0.EF0eMYwPq0ItKw07Q2y4iIbRosJEfaVCgQme2uHxv7IqWJydtoPh_qvSu8FXQaUjFzTI1UzltVG3FIUC6xTKb1g29xzgTq2knZLxUZzZox3VD4U2-3npFu4ZqIEeM8tr1ijMakmWkqy4cge2RypUgRWOhjr642kInGbOqs0G6nREjNvON71sh2wovrEjC2naIvP9naKzGQ3U77qRrUnJzVFUBRjXW0-5kPH6j7-SamS-AjHXkA2rqi9f_Jmr2VNskPL4ehYzySdhASbslfLNXNicP8921CWLhdjAUu3jQ8PkS_CDFlKH2HAUuVlGeP1KR07i79ISjFfkyXei1beQPg`

func TestAuthencitation(t *testing.T) {
	publicKey := publicKey{Kid: "kid", UserId: "userid", Pk: "pk"}

	// Marshal publicKey to json
	jsonPublicKey, err := json.Marshal(publicKey)
	if err != nil {
		t.Errorf("Expected nil error, got %v", err)
	}

	// Create a new request with the jsonPublicKey as the request body
	req := httptest.NewRequest("POST", "/pk", bytes.NewBuffer(jsonPublicKey))

	w := httptest.NewRecorder()
	PostPublicKey(w, req)
	res := w.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Expected nil error, got %v", err)
	}
	result := strings.TrimSpace(string(data))

	if result != "Access denied" {
		t.Errorf(`Expected 'Access denied', got %v`, result)
	}
}

// addValidPublicKey tests that a valid public key is added to the database.
// FIXME: This currently does not test that the public key was added to the database. It only tests that the function tries to connect to the database.
func TestAddValidPublicKey(t *testing.T) {
	publicKey := publicKey{Kid: "kid", UserId: "userid", Pk: "pk"}

	// Marshal publicKey to json
	jsonPublicKey, err := json.Marshal(publicKey)
	if err != nil {
		t.Errorf("Expected nil error, got %v", err)
	}

	// Create a new request with the jsonPublicKey as the request body
	req := httptest.NewRequest("POST", "/pk", bytes.NewBuffer(jsonPublicKey))
	req.Header.Add("Authorization", "Bearer "+idToken)

	w := httptest.NewRecorder()
	PostPublicKey(w, req)
	res := w.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Expected nil error, got %v", err)
	}
	result := strings.TrimSpace(string(data))

	// FIXME: this should always fail because it cannot connect to OIDC provider. It would be nice to be able to mock it
	if result != `ID token not valid` {
		t.Errorf(`Expected 'ID token not valid', got %v`, result)
	}

}

func TestAddInvalidPublicKey(t *testing.T) {
	req := httptest.NewRequest("POST", "/pk", nil)
	req.Header.Add("Authorization", "Bearer "+idToken)

	w := httptest.NewRecorder()
	PostPublicKey(w, req)
	res := w.Result()
	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Expected nil error, got %v", err)
	}
	result := strings.TrimSpace(string(data))

	// FIXME: this should always fail because it cannot connect to OIDC provider. It would be nice to be able to mock it.
	if result != "ID token not valid" {
		t.Errorf("Expected 'ID token not valid', got %v", result)
	}
}

func TestGetMyPublicKeysWrongToken(t *testing.T) {
	req := httptest.NewRequest("GET", "/mykeys", nil)
	req.Header.Add("Authorization", "Bearer wrongtoken")

	w := httptest.NewRecorder()
	GetMyPublicKeys(w, req)
	res := w.Result()
	defer res.Body.Close()

	// Check status code
	if res.StatusCode != http.StatusUnauthorized {
		t.Errorf("Expected %v, got %v", http.StatusUnauthorized, res.StatusCode)
	}
}

func TestGetMyPublicKeysNoToken(t *testing.T) {
	req := httptest.NewRequest("GET", "/mykeys", nil)

	w := httptest.NewRecorder()
	GetMyPublicKeys(w, req)
	res := w.Result()
	defer res.Body.Close()

	// Check status code
	if res.StatusCode != http.StatusUnauthorized {
		t.Errorf("Expected %v, got %v", http.StatusUnauthorized, res.StatusCode)
	}
}

func TestGetPublicKey(t *testing.T) {
	// Missing query parameters
	req := httptest.NewRequest("GET", "/pk", nil)

	w := httptest.NewRecorder()
	GetPublicKey(w, req)
	resNoParams := w.Result()
	defer resNoParams.Body.Close()

	// Check status code
	if resNoParams.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, resNoParams.StatusCode)
	}

	// missing kid
	req = httptest.NewRequest("GET", "/pk?userid=userid", nil)
	w = httptest.NewRecorder()
	GetPublicKey(w, req)
	resNoKid := w.Result()
	defer resNoKid.Body.Close()

	// Check status code
	if resNoKid.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, resNoKid.StatusCode)
	}

	// missing userid
	req = httptest.NewRequest("GET", "/pk?kid=kid", nil)
	w = httptest.NewRecorder()
	GetPublicKey(w, req)
	resNoUserId := w.Result()
	defer resNoUserId.Body.Close()

	// Check status code
	if resNoUserId.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, resNoUserId.StatusCode)
	}

}
