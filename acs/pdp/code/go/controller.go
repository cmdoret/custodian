/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package pdp

import (
	"bytes"
	"custodian/acs/pdp/config"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	custodian "gitlab.com/data-custodian/custodian-go/models"
	util "gitlab.com/data-custodian/custodian-go/util"

	"github.com/golang-jwt/jwt"
)

type ResponseStruct struct {
	Resp string `json:"resp"`
}

// FIXME: Should PAPResponse contains several possible contract triads?
type PAPResponse struct {
	Resp           string         `json:"resp"`
	ContractsTriad ContractsTriad `json:"contracts"`
}

// ContractsTriad contains a contract signature and the contract it makes reference to, as well as the string version of the contract
type ContractsTriad struct {
	ContractString custodian.ContractString `json:"contract_string"`
	ContractBody   custodian.ContractBody   `json:"contract_body"`
	Signature      custodian.Signature      `json:"contract_signature"`
}

type tags map[string][]string

type authorizationContext struct {
	Pep   string   `json:"pep"`
	Actor string   `json:"actor"`
	Owner string   `json:"owner"`
	Scope []string `json:"scope"`
}

const (
	TIMEOUT = 30 * time.Second
)

var (
	current_id uint64 = 0 //FIXME: Store this somewhere else
)

// GetAuthorization verifies if a contract corresponds to the parameters, if it has been signed and if it is valid.
//
// The parameters t (tags), q (context) and u (unix timestamp) are extracted from the query URL.
// The PAP is queried using the tags `t`, and a contract and the corresponding signature are retreived (or not).
// The actor and user IDs are extracted from the context `q`, and used to validate the contract and its signature.
//
// If the contract is valid, the function returns a JWT. Otherwise, it returns an error message.
func GetAuthorization(w http.ResponseWriter, r *http.Request) {
	tQuery, ok := r.URL.Query()["t"]
	if !ok {
		encodeResponse(w, "Missing 't' parameter (tags)", http.StatusBadRequest)
		return
	}

	decodedTags, err := url.QueryUnescape(tQuery[0])
	if err != nil {
		encodeResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	qQuery, ok := r.URL.Query()["q"]
	if !ok {
		encodeResponse(w, "Missing 'q' parameter (context)", http.StatusBadRequest)
		return
	}

	decodedContext, err := url.QueryUnescape(qQuery[0])
	if err != nil {
		encodeResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	uQuery, ok := r.URL.Query()["u"]
	if !ok {
		encodeResponse(w, "Missing 'u' parameter (unix time)", http.StatusBadRequest)
		return
	}
	u, err := strconv.Atoi(uQuery[0])
	if err != nil {
		encodeResponse(w, err.Error(), http.StatusBadRequest)
		return
	}

	decodedTime := time.Unix(int64(u), 0)
	if err != nil {
		encodeResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Println("New query.")
	log.Println("Tags:", decodedTags)
	log.Println("Context:", decodedContext)
	log.Println("Query timestamp:", decodedTime)

	var tags tags
	err = json.Unmarshal([]byte(decodedTags), &tags)
	if err != nil {
		encodeResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// FIXME: tags should not be hardwired
	if _, ok := tags["userid"]; !ok || len(tags["userid"]) == 0 {
		http.Error(w, "Please provide at least one user id in the tags", http.StatusBadRequest)
	}
	if _, ok := tags["clientid"]; !ok || len(tags["clientid"]) == 0 {
		http.Error(w, "Please provide at least one client id in the tags", http.StatusBadRequest)
	}

	var authorizationContext authorizationContext
	err = json.Unmarshal([]byte(decodedContext), &authorizationContext)
	if err != nil {
		encodeResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if authorizationContext.Actor == "" {
		encodeResponse(w, "Missing context parameter: 'actor' (string)", 400)
		return
	} else if authorizationContext.Owner == "" {
		encodeResponse(w, "Missing context parameter: 'owner' (string)", 400)
		return
	} else if len(authorizationContext.Scope) == 0 {
		encodeResponse(w, "Missing context parameter: 'scope' (list of strings)", 400)
		return
	}

	// Get the contract through PAP

	PAPUrl := "http://" + config.C.PAP.Hostname + ":" + config.C.PAP.Port + config.C.PAP.ComponentUrlPath

	log.Printf("Query sent to PAP: %s/triad?t=%s\n", PAPUrl, tQuery[0])
	resp, err := http.Get(fmt.Sprintf("%s/triad?t=%s", PAPUrl, url.QueryEscape(tQuery[0])))
	if err != nil {
		log.Println(err.Error())
		encodeResponse(w, "Server error", 500)
		return
	} else if resp.StatusCode != 200 {
		switch resp.StatusCode {
		case http.StatusNotFound:
			encodeResponse(w, "Contract not found", 404)
		default:
			encodeResponse(w, "Server error", 500)
		}
		return
	}
	defer resp.Body.Close()

	var papResponse PAPResponse

	timestamp := time.Now()

	err = json.NewDecoder(resp.Body).Decode(&papResponse)
	if err != nil {
		log.Println("Could not decode PAP response:", err.Error())
		sendErrorEvent(tags, authorizationContext, "unauthorized")
		encodeResponse(w, "Contract not found", 404)
		return
	}

	if papResponse.Resp != "ok" {
		log.Println("Contract does not exist")
		sendErrorEvent(tags, authorizationContext, "unauthorized")
		encodeResponse(w, "Contract not found", 404)
		return
	}

	log.Println("Contract found.")

	contracts := papResponse.ContractsTriad

	// TODO: Verify contract
	// 1. Get CMS public key (TODO)
	// 2. Verify signature
	// 3. Check the contract is valid

	if contracts.ContractString == (custodian.ContractString{}) { // FIXME: verify that the PAP response is not nil?
		log.Println("Contract not found")
		sendErrorEvent(tags, authorizationContext, "unauthorized")
		encodeResponse(w, "Contract not found", 404)
		return
	} else {
		contractSignature := contracts.Signature
		signatureIsValid, err := util.VerifyLastContractSignature(contractSignature, config.C.CmsPublicKey)
		if err != nil {
			encodeResponse(w, "Error: "+err.Error(), 500)
			return
		} else if !signatureIsValid {
			sendErrorEvent(tags, authorizationContext, "unauthorized")
			encodeResponse(w, "Signature not valid", 401)
			return
		}
	}

	if decodedTime.Add(TIMEOUT).Before(time.Now()) {
		encodeResponse(w, "Request timed out", 408)
		return
	} else if !isInTimeRange(contracts.ContractBody, decodedTime) {
		sendErrorEvent(tags, authorizationContext, "contract expired")
		encodeResponse(w, "Contract expired", 408)
		return
	}

	// We check that the required subjects signed the contract
	valid := checkSubjectsApproval(contracts.ContractBody, contracts.Signature)
	if !valid {
		sendErrorEvent(tags, authorizationContext, "unauthorized")
		encodeResponse(w, "Missing signature(s)", 401)
		return
	}

	// If user signed a contract to store this type of data, approve and return a token

	scopeAndConstraints := getAllowedScope(contracts.ContractBody, authorizationContext.Scope, authorizationContext.Actor, authorizationContext.Owner)

	if len(scopeAndConstraints) == 0 {
		sendErrorEvent(tags, authorizationContext, "unauthorized")
		encodeResponse(w, "Unauthorized", 401)
		return
	}

	var scope []string
	for _, constraint := range scopeAndConstraints {
		scope = append(scope, constraint.Function)
	}

	claims := custodian.TokenClaims{
		UserID:      authorizationContext.Owner,
		ClientID:    tags["clientid"][0], //FIXME: hardcoded key, we must make sure it is there
		SignatureID: contracts.Signature.OID,
		Actor:       authorizationContext.Actor,
		Scope:       scope, // FIXME: list of ScopeStruct.Verb
		Constraints: scopeAndConstraints,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  timestamp.Unix(),
			ExpiresAt: timestamp.Add(time.Minute * 15).Unix(),
			Issuer:    "mf.testing.swisscustodian.ch",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["kid"] = config.C.Kid // FIXME: change config key name
	signedToken, err := token.SignedString(config.C.PrivateKey)

	log.Println("Token generated:", token)

	fmt.Fprintf(w, "%v", signedToken)

	// FIXME: send log to MQ
	// E.g.:
	// User UserOID: Access granted to contractAuthorOID for: fields[0], fields[1], ...

	//actionLog := custodian.ActionLog{oid, fields, [], contractAuthorOID, time.Now()}

	// FIXME: this is a sample event
	err = sendEvent(timestamp.Unix(), signedToken, "contract", "grant access", map[string]interface{}{"contract_id": contracts.ContractBody.OID})
	if err != nil {
		encodeResponse(w, "Server error", 500)
		return
	}
}

// sendErrorEvent creates a message that contains the error message and sends it to the message queue
func sendErrorEvent(tags tags, authorizationContext authorizationContext, errorMessage string) {
	claims := custodian.TokenClaims{
		UserID:      authorizationContext.Owner,
		ClientID:    tags["clientid"][0], //FIXME: hardcoded key, we must make sure it is there
		SignatureID: "",
		Actor:       authorizationContext.Actor,
		Scope:       nil, // FIXME: should be a list of ScopeStruct.Verb
		Constraints: nil,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
			Issuer:    "mf.testing.swisscustodian.ch", // FIXME: hardcoded
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)
	token.Header["kid"] = config.C.Kid // FIXME: change config key name

	if config.C.PrivateKey == nil {
		log.Println("Private key is nil")
		return
	}
	signedToken, err := token.SignedString(config.C.PrivateKey)
	if err != nil {
		log.Println(err)
		return
	}

	err = sendEvent(time.Now().Unix(), signedToken, "contract", "deny access", map[string]interface{}{"error": errorMessage, "actor": authorizationContext.Actor, "owner": authorizationContext.Owner, "scope": fmt.Sprintf("%v", authorizationContext.Scope)})
	if err != nil {
		log.Println(err)
		return
	}
}

// sendEvent sends a message to the events message queue
func sendEvent(timestamp int64, token string, resource string, action string, attribute map[string]interface{}) error {
	event := custodian.Event{
		Unix:      timestamp,
		Token:     token,
		ID:        fmt.Sprintf("%d", current_id),
		Resource:  resource,
		Action:    action,
		Attribute: attribute,
	}

	jsonEvent, err := json.Marshal(event)
	if err != nil {
		log.Println("Error:", err)
		return err
	}
	current_id += 1

	resp, err := http.Post(config.C.AuditTrailUrl, "application/json", bytes.NewBuffer(jsonEvent))
	if err != nil {
		return err
	} else if resp.StatusCode != 200 {
		return fmt.Errorf("Audit Trail replied with status %d", resp.StatusCode)
	}

	log.Println("Event sent. Audit Trail replied with status", resp.StatusCode)
	return nil
}

// checkSubjectsApproval verifies that all required signatures are present.
func checkSubjectsApproval(contractBody custodian.ContractBody, contractSignature custodian.Signature) bool {
	// FIXME: the function does not take all necessary information into account. For example, it does not check the validity of the signature, nor that the signature is for the correct contract.
	varMap := make(map[string]string)
	for _, contractVar := range contractBody.Var {
		varID := strings.TrimLeft(contractVar.ID, contractBody.OID)
		varMap[varID] = contractVar.Value
	}
	var signees []string
	for _, seal := range contractSignature.Seal {
		signees = append(signees, strings.Split(seal.Signee, "?")[0]) // FIXME: also split "?" for var values?
	}

	for _, requiredSignee := range contractBody.Validity.Sign {
		if signeeValue, ok := varMap[requiredSignee]; !ok || !contains(signees, signeeValue) {
			log.Println(signeeValue, "did not sign the contract.")
			return false
		}
	}

	return true
}

// isInTimeRange verifies if `timestamp` is within the contract validity time span.
func isInTimeRange(contractBody custodian.ContractBody, timestamp time.Time) bool {
	return !timestamp.Before(contractBody.Validity.NotBefore) && !timestamp.After(contractBody.Validity.NotAfter) // We make sure it is not before and not after
}

// contains returns true if `str` is in `list`, and false otherwise.
func contains(list []string, str string) bool {
	for _, element := range list {
		if str == element {
			return true
		}
	}
	return false
}

// findSubjectRef returns the "subject reference" (e.g., #subject1) that corresponds to an actor DID in the contract.
func findSubjectRef(contractBody custodian.ContractBody, actor string) string {
	var varName string
	actorDid := strings.Split(actor, "?")[0] // We remove the "?kid=" part in case there is one
	for _, contractVar := range contractBody.Var {
		if contractVar.Value == actorDid {
			varName = strings.TrimLeft(contractVar.ID, contractBody.OID)
		}
	}

	for _, subject := range contractBody.Subject {
		if oid, ok := subject.Attribute["id"]; ok && oid == varName { // FIXME: OID or ID?
			return strings.TrimLeft(subject.ID, contractBody.OID)
		}
	}

	return ""
}

// findObject returns an object based on its reference in the contract (e.g., #object1)
func findObject(contractBody custodian.ContractBody, objectRef string) custodian.Object {
	if len(objectRef) == 0 {
		return custodian.Object{}
	}
	for _, object := range contractBody.Object {
		id := object.ID
		if string(objectRef[0]) == "#" {
			id = strings.TrimLeft(id, contractBody.OID)
		}
		if id == objectRef {
			return object
		}
	}

	return custodian.Object{}
}

// getAllowedScope returns a subset of the `functions` that `actor` is allowed to perform on `provider` data by the contract `contractBody`.
func getAllowedScope(contractBody custodian.ContractBody, functions []string, actor string, provider string) []custodian.Constraint {
	verbs := make(map[string]string)
	for _, function := range functions {
		for _, verb := range contractBody.Verb {
			if function == verb.Function {
				verbs[strings.TrimLeft(verb.ID, contractBody.OID)] = function
			}
		}
	}

	// We get the references of the subject and the owner in the contract (e.g., #subject-3, #subject-8)
	subject := findSubjectRef(contractBody, actor)
	owner := findSubjectRef(contractBody, provider)

	log.Println("Query:", actor, "wants to access", subject, "data")

	var allowedScopes []custodian.Constraint
	for _, scope := range contractBody.Scope {
		if contains(scope.Subject, subject) {
			// Verify that `provider` owns at least one of the objects
			objectIn := false
			var constraintAttributes map[string]interface{}

			for _, objectRef := range scope.Object {
				object := findObject(contractBody, objectRef)

				if object.Owner == owner {
					objectIn = true
					constraintAttributes = object.Attribute
					break
				}
			}
			if objectIn {
				for _, verb := range scope.Verb {
					if function, ok := verbs[verb]; ok {
						log.Println("Allowed function:", function)
						log.Println("Constraints:", constraintAttributes)
						allowedScopes = append(allowedScopes, custodian.Constraint{function, provider, constraintAttributes})
					}
				}
			}
		}
	}

	return allowedScopes
}

// encodeResponse encodes `text` in json, and sends it to the response writer with the status code `statusCode`
func encodeResponse(w http.ResponseWriter, text string, statusCode int) {
	w.WriteHeader(statusCode)
	data := ResponseStruct{text}
	e := json.NewEncoder(w)
	e.Encode(data)
}

// parameterArrayToURLString translates `array` into its URL format
func parameterArrayToURLString(parameter string, array []string) string {
	str := parameter + "="

	for i, item := range array {
		str += item
		if i != len(array)-1 {
			str += "&" + parameter + "="
		}
	}

	return str
}
