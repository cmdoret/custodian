# Policy Decision Point

The Policy Decision Point (PDP) interprets the contracts and signature to decide whether an actor has the right to access/process some data.

## Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

### Configuration

The config file can be found in `config/config.yml`. For the new configuration to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in this folder (`custodian/acs/pdp`) and run the following command:
```
go build -o pdp code/main.go
./pdp
```

## Usage

The list of endpoints and their description can be found in `api/openapi.yml`

