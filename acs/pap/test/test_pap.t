test no_oid:
  $ RESP=$(curl -s --show-error "$PAP_HOST:$PAP_PORT")
  $ echo $RESP
  { "resp": "OID not valid", "contract": {} }

test no_fields:
  $ RESP=$(curl -s --show-error "$PAP_HOST:$PAP_PORT?oid=abc")
  $ echo $RESP
  { "resp": "Author OID not valid", "contract": {} }

test get:
  $ RESP=$(curl -s --show-error "$PAP_HOST:$PAP_PORT?oid=abc&contract_author_oid=aaa")
  $ echo $RESP
  { "resp": "Fields not valid", "contract": {} }

