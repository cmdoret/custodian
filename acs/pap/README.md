# PAP

A dummy PAP sending consents to the PDP.

Currently, consents are hardcoded consents stored in local variables.

# Installation

### The Dependencies

The code has three dependencies: `gorilla-mux`, `mongo-driver` and `viper`

To install them, open a terminal and execute the following commands:

```
go get github.com/gorilla/mux
go get -u go.mongodb.org/mongo-driver/mongo
go get github.com/spf13/viper
```

### Build and Run

To build and run the code, open a terminal in the `custodian/acs/pap` folder and run the following command:
```
.../custodian/acs/pap$ go build -o PAP main.go functions.go
.../custodian/acs/pap$ ./pap
```

# Usage
## End Points

### Get Consent
* URL: `http://localhost:8012/`
* Description: This end point can be used to retrieve a consent. 
* URL Parameters: 
    * `oid`: OID of the data "owner" (the user uploading the data)
    * `fields`: the name of the fields that must be described in the consent (e.g. `latitude`, `longitude`, etc.). If there are multiple fields, use the following format: `http://localhost/?oid=123&fields=latitude&fields=longitude&...`


