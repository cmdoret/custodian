# Access Control

This folder contains the code for the Policy Decision Point service and the Policy Administration Point service.

* Policy Decision Point: evaluates access requests from Policy Enforcement Point service against authorization policies (contracts) before granting access authorization.
* Policy Administration Point: retrieves contracts and signature in the contract database.
* Eventhandler: processes the contracts and signatures message queues and performs event sourcing to recreate a view of the contracts

When the command handler (Policy Enforcement Point) receives the task to access through from the custodian, it must verify that the user signed a contract for it. 

For that, the Policy Enforcement Point service sends the tags (`t`), context (`q`) and current timestamp (`u`) to the Policy Decision Point.

The tags field must contain:
* The IDs of the different users involved
* The ID of the client

The context field must contain at least:

* The data owner's ID
* The actor's ID
* The scope

More information about the APIs can be found in `pdp/api/openapi.yml`.

The PDP sends this data to the PAP, which will search for a contract and a signature matching the information provided in the tags and context. If the PAP finds such a contract, it sends it to the PDP, which verifies its validity before approving the action of the PEP.

Currently, we do not use the PRP. This will be implemented later.

----

# How to Run the Service With Docker Compose


Before running docker-compose, you must configure the environment variables in the `docker-compose.yml` file.

Create a folder with the secret files and reference them in the `docker-compose.yml` file.

Then, you can run the following command in a terminal:

```
docker compose up --build
```
