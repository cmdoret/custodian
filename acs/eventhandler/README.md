# Access Control Event Handler

The ACS event handler reads the events in the contracts message queue and recreates a view of the authorizations.

# Installation

### The Dependencies

The code has been built with `Go 1.19.2`, but should work with any version that supports Go modules.

To install the dependencies, open a terminal in the `code` folder and type

```
go get
```

### Configuration

The config file can be found in `config/config.yml`. For the new configuration to take effect, you must rerun the service.

### Build and Run

To build and run the code, open a terminal in this folder (`custodian/acs/eventhandler`) and run the following command:
```
go build -o eventhandler code/main.go
./eventhandler
```
