version: '3.2'
services:
    accesscontrol-pdp:
        image: $CI__REGISTRY_IMAGE_SUFFIX/accesscontrol-pdp:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: acs/pdp/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        ports:
            - "8011:8011"
        environment:
            - SERVER_HOSTNAME=0.0.0.0
            - SERVER_PORT=8011
            - SERVER_COMPONENTURLPATH=/acs
            - AUDITTRAILURL=http://audit-trail-receiver:8015/audit-trail
            - PAP_HOSTNAME=accesscontrol-pap
            - PAP_PORT=8012
            - PAP_COMPONENTURLPATH=/acs-pap
            - IMS_HOSTNAME=ims
            - IMS_PORT=8007
            - IMS_COMPONENTURLPATH=/ims
            - PRIVATEKEYBLOCATION=/run/secrets/ecdsa/pdp/private.pem
            - CMSPUBLICKEYLOCATION=/run/secrets/ecdsa/cms/public.pem
        secrets:
            - source: pdp-ssl-key
              target: ssl/key/custodian-acs-pdp.key
            - source: pdp-ssl-crt
              target: ssl/certs/custodian-acs-pdp.crt
            - source: pdp-ecdsa-private
              target: ecdsa/pdp/private.pem
            - source: cms-ecdsa-public
              target: ecdsa/cms/public.pem
            - source: custodian-ca
              target: ssl/ca/custodianCA.crt
        labels:
            kompose.service.expose: ${CI__PREFIX}${CI__CUSTODIAN_DOMAIN}/acs
            kompose.service.expose.tls-secret: tls-swissdatacustodian-with-ca-2023
            #kompose.service.type: nodeport
            ##kompose.volume.type: configMap
            ##kompose.volume.storage-class-name: longhorn
        extra_hosts:
            - "ims:172.17.0.1" # FIXME: kompose does not understand this

    accesscontrol-pap:
        image: $CI__REGISTRY_IMAGE_SUFFIX/accesscontrol-pap:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: acs/pap/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        ports:
            - "8012:8012"
        environment:
            - SERVER_HOSTNAME=0.0.0.0
            - SERVER_PORT=8012
            - SERVER_COMPONENTURLPATH=/acs-pap
            - MONGODB_URL=mongodb://platform-mongodb27017:27017 # as exeternalmongodb is not understood by kompose, we put directly put mongodb27017
        extra_hosts:
            - "platform-mongodb27017:172.17.0.1" # FIXME: kompose does not understand this
        labels:
            kompose.service.expose: ${CI__PREFIX}${CI__CUSTODIAN_DOMAIN}/acs-pap
            kompose.service.expose.tls-secret: tls-swissdatacustodian-with-ca-2023

    accesscontrol-eventhandler:
        image: $CI__REGISTRY_IMAGE_SUFFIX/accesscontrol-eventhandler:$CI__IMAGE_TAG
        build:
            context: ../
            dockerfile: acs/eventhandler/Dockerfile
            args:
                CI__REGISTRY_IMAGE: $CI__REGISTRY_IMAGE
                CI__GO_IMAGE: $CI__GO_IMAGE
                CI__IMAGE_TAG: $CI__IMAGE_TAG
                CI__BASE_TAG: $CI__BASE_TAG
                CI__CONTAINER_IMAGE: $CI__CONTAINER_IMAGE
        environment:
            - PULSAR_URL=pulsar://platform-pulsar6650:6650 ## FIXME: eventhandler depends on pulsar, which belongs to another docker compose file (cms)
            - PULSAR_MAXRETRIES=10
            - PULSAR_SLEEPTIME=15s
            - MONGODB_URL=mongodb://platform-mongodb27017:27017
        extra_hosts:
            - "platform-mongodb27017:172.17.0.1" # FIXME: kompose does not understand this
            - "platform-pulsar6650:172.17.0.1" # FIXME: kompose does not understand this

secrets:
    pdp-ssl-key:
        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/acs/pdp/ssl/custodian-acs-pdp.key
    pdp-ssl-crt:
        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/acs/pdp/ssl/custodian-acs-pdp.crt
    pdp-ecdsa-private:
        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/acs/pdp/ecdsa/private.pem
    cms-ecdsa-public:
        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/cms/signature/ecdsa/public.pem
    custodian-ca:
        file: ${CI__SRC_PATH:-.}/custodian-internal/secrets/ca/custodianCA.crt ##FIXME: should not be in acs/pdp, but in CA's folder
