/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/zitadel/oidc/v2/pkg/client/rp"
	"github.com/zitadel/oidc/v2/pkg/oidc"

	"github.com/labstack/echo/v4"
)

// checkAuthorizationHeader checks if the request contains a Authorization Bearer token. It does not validate the token.
func checkAuthorizationHeader(h http.Header) bool {
	auth := h.Get("Authorization")
	if auth == "" {
		return false
	}
	if !strings.HasPrefix(auth, oidc.PrefixBearer) {
		return false
	}

	return true
}

func validateIDToken(ctx context.Context, idToken string) bool {

	_, err := rp.VerifyIDToken[*oidc.IDTokenClaims](ctx, idToken, IDTokenVerifier)
	if err != nil {
		return false
	}

	return true
}

func readEncryptedCookie(cookie http.Cookie) (string, error) {

	var value string
	err := secureCookie.Decode(cookie.Name, cookie.Value, &value)
	if err != nil {
		return "", err
	}
	return value, nil
}

// redirectToLoginPage redirects the user to the login page.
// Beforehand, the current URL is stored in a cookie.
func redirectToLoginPage(c echo.Context) error {
	cookie := new(http.Cookie)
	cookie.Name = RedirectionCookieName
	cookie.Value = c.Request().URL.String()
	cookie.Path = "/auth/callback"
	cookie.Expires = time.Now().Add(1 * time.Minute)
	cookie.HttpOnly = true
	c.SetCookie(cookie)
	return c.Redirect(http.StatusSeeOther, "/auth/login")
}

// authorize is a middleware that verifies that the request contains a Authorization Bearer token.
// If not, it displays an error message.
func authorize() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			ok := checkAuthorizationHeader(c.Request().Header)
			if !ok {
				return echo.NewHTTPError(http.StatusUnauthorized, "Please provide a bearer token")
			}
			return next(c)
		}
	}
}

// authenticate is a middleware that verifies that the user is authenticated with a valid cookie.
// If not, it redirects the user to the login page.
func authenticate() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cookie, err := c.Request().Cookie(IDTokenCookieName)
			if err != nil {
				switch err {
				case http.ErrNoCookie:
					// Redirect to login page
					return redirectToLoginPage(c)
				default:
					return err
				}
			}
			cookieValue, err := readEncryptedCookie(*cookie)
			if err != nil {
				cookie.MaxAge = -1
				c.SetCookie(cookie)
				return redirectToLoginPage(c)
			}

			idToken := cookieValue
			ok := validateIDToken(c.Request().Context(), idToken)
			if !ok {
				// Redirect to login page
				return redirectToLoginPage(c)
			}

			// Set Authorization header and continue
			c.Request().Header.Add("Authorization", fmt.Sprintf("%s%s", oidc.PrefixBearer, idToken))

			return next(c)
		}
	}
}
