/**
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"net/http"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/zitadel/oidc/v2/pkg/client/rp"
	httphelper "github.com/zitadel/oidc/v2/pkg/http"
	"github.com/zitadel/oidc/v2/pkg/oidc"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"github.com/gorilla/securecookie"
)

var (
	hashKey  = securecookie.GenerateRandomKey(64)
	blockKey = securecookie.GenerateRandomKey(32)

	secureCookie = securecookie.New(hashKey, blockKey)

	IDTokenVerifier rp.IDTokenVerifier
)

const (
	IDTokenCookieName     = "SWISSDATACUSTODIAN_IDTOKEN"
	RedirectionCookieName = "SWISSDATACUSTODIAN_REDIRECT"
)

// store the IDToken in a cookie
func storeIDToken(w http.ResponseWriter, r *http.Request, tokens *oidc.Tokens[*oidc.IDTokenClaims], state string, rp rp.RelyingParty) {
	// FIXME: Should we validate it?
	// FIXME: Store in redis instead?
	encodedCookie, err := secureCookie.Encode(IDTokenCookieName, tokens.IDToken)
	if err != nil {
		logrus.Errorf("Error encoding cookie.")
		return
	}
	cookie := new(http.Cookie)
	cookie.Name = IDTokenCookieName
	cookie.Value = encodedCookie
	cookie.Expires = tokens.IDTokenClaims.Expiration.AsTime()
	cookie.HttpOnly = true
	cookie.Path = "/"
	//FIXME: add secure (and other important cookies flags)
	//cookie.Secure = true
	http.SetCookie(w, cookie)
}

func main() {
	err := ReadConfig()
	if err != nil {
		logrus.Fatalf("error reading config %s", err.Error())
	}

	clientID := config.OIDC.ClientID
	clientSecret := config.OIDC.ClientSecret
	issuer := config.OIDC.Issuer
	scopes := config.OIDC.Scopes
	redirectURI := config.OIDC.Callback

	keyPath := os.Getenv("KEY_PATH")

	cookieHandler := httphelper.NewCookieHandler(hashKey, blockKey, httphelper.WithUnsecure())

	options := []rp.Option{
		rp.WithCookieHandler(cookieHandler),
		rp.WithVerifierOpts(rp.WithIssuedAtOffset(5 * time.Second)),
	}
	if clientSecret == "" {
		options = append(options, rp.WithPKCE(cookieHandler))
	}
	if keyPath != "" {
		options = append(options, rp.WithJWTProfile(rp.SignerFromKeyPath(keyPath)))
	}

	var provider rp.RelyingParty
	for i := 0; i < config.OIDC.MaxRetries; i++ {
		provider, err = rp.NewRelyingPartyOIDC(issuer, clientID, clientSecret, redirectURI, scopes, options...)
		if err != nil {
			logrus.Errorf("error creating provider: %s", err.Error())
			logrus.Errorf("retrying in %s", config.OIDC.SleepTime.String())
			time.Sleep(config.OIDC.SleepTime)
		} else {
			break
		}
	}

	// create a verifier for the IDToken
	IDTokenVerifier = provider.IDTokenVerifier()

	// generate some state (representing the state of the user in your application,
	// e.g. the page where he was before sending him to login
	state := func() string {
		return uuid.New().String()
	}

	e := echo.New()

	auth := e.Group("/auth")
	auth.Use(middleware.Logger())
	auth.Use(middleware.Recover())
	auth.GET("/login", echo.WrapHandler(rp.AuthURLHandler(state, provider, rp.WithPromptURLParam("Welcome back!"))))
	auth.GET("/callback", func(c echo.Context) error {
		handlerFunc := rp.CodeExchangeHandler(storeIDToken, provider)
		handlerFunc(c.Response(), c.Request())

		// Get the redirect URL from the cookie.
		cookie, err := c.Cookie(RedirectionCookieName)
		if err != nil {
			switch err {
			case http.ErrNoCookie:
				// If the cookie is not set, redirect to the index page.
				return c.Redirect(http.StatusFound, "/")
			default:
				return err
			}
		}

		url := cookie.Value

		// Delete the cookie.
		cookie.MaxAge = -1
		c.SetCookie(cookie)

		return c.Redirect(http.StatusFound, url)
	})

	// Set the redirections according to the config
	for _, redirection := range config.Redirections {
		SetProxyPrefix(e, redirection.Path, redirection.Scheme, redirection.Host,
			middleware.Logger(),
			middleware.Recover(),
			authenticate())
	}

	e.Logger.Fatal(e.Start(config.Server.Hostname + ":" + config.Server.Port))

}
