/*
 * Swiss Data Custodian provides mechanisms for contract management and access control.
 * Copyright (C) 2019 - Swiss Data Science Center
 * A partnership between École Polytechnique Fédérale de Lausanne (EPFL) and
 * Eidgenössische Technische Hochschule Zürich (ETHZ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"strings"
	"time"

	"github.com/spf13/viper"
)

type configStruct struct {
	// Reverse proxy redirections
	Redirections []redirection `yaml:"Redirections"`
	OIDC         oidcConfig    `yaml:"OIDC"`
	Server       serverConfig  `yaml:"Server"`
}

type redirection struct {
	Path   string `yaml:"Path"`
	Scheme string `yaml:"Scheme"`
	Host   string `yaml:"Host"`
}

type serverConfig struct {
	// Server configuration
	Hostname string `yaml:"Hostname"`
	Port     string `yaml:"Port"`
}

// OIDC configuration
type oidcConfig struct {
	ClientID     string   `yaml:"ClientID"`
	ClientSecret string   `yaml:"ClientSecret"`
	Issuer       string   `yaml:"Issuer"`
	Callback     string   `yaml:"Callback"`
	Scopes       []string `yaml:"Scopes"`
	// Max number of retries to connect to the OIDC issuer
	MaxRetries int `yaml:"MaxRetries"`
	// Time to wait before each retry. Use a valid `time.ParseDuration` input (e.g., `5s` for 5 seconds, `150ms` for 150 milliseconds, etc.)
	SleepTime time.Duration `yaml:"SleepTime"`
}

// C is used to store the values configured in config.yml
var config configStruct

func ReadConfig() error {
	Config := &config
	viper.SetConfigName("config") // name of the config file
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if err := viper.Unmarshal(&Config); err != nil {
		return err
	}

	return nil
}
