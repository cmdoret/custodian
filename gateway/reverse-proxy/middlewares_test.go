package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/zitadel/oidc/v2/pkg/oidc"
)

const (
	idToken  = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImFhYSI6NjkyfQ.eyJhY3IiOiJzb21ldGhpbmciLCJhbXIiOlsiZm9vIiwiYmFyIl0sImF0X2hhc2giOiIyZHpibV92SXh5LTdlUnRxVUlHUFB3IiwiYXVkIjpbInVuaXQiLCJ0ZXN0IiwiNTU1NjY2Il0sImF1dGhfdGltZSI6MTY3ODEwMDk2MSwiYXpwIjoiNTU1NjY2IiwiYmFyIjp7ImNvdW50IjoyMiwidGFncyI6WyJzb21lIiwidGFncyJdfSwiY2xpZW50X2lkIjoiNTU1NjY2IiwiZXhwIjo0ODAyMjM4NjgyLCJmb28iOiJIZWxsbywgV29ybGQhIiwiaWF0IjoxNjc4MTAxMDIxLCJpc3MiOiJsb2NhbGhvc3Q6NDQ0NCIsImp0aSI6Ijk4NzYiLCJuYmYiOjE2NzgxMDEwMjEsIm5vbmNlIjoiMTIzNDUiLCJzdWIiOiJ0aW1AbG9jYWwuY29tIn0.EF0eMYwPq0ItKw07Q2y4iIbRosJEfaVCgQme2uHxv7IqWJydtoPh_qvSu8FXQaUjFzTI1UzltVG3FIUC6xTKb1g29xzgTq2knZLxUZzZox3VD4U2-3npFu4ZqIEeM8tr1ijMakmWkqy4cge2RypUgRWOhjr642kInGbOqs0G6nREjNvON71sh2wovrEjC2naIvP9naKzGQ3U77qRrUnJzVFUBRjXW0-5kPH6j7-SamS-AjHXkA2rqi9f_Jmr2VNskPL4ehYzySdhASbslfLNXNicP8921CWLhdjAUu3jQ8PkS_CDFlKH2HAUuVlGeP1KR07i79ISjFfkyXei1beQPg`
	testPath = "/test"
)

func setTestEnvVars(t *testing.T) {
	t.Setenv("CLIENT_ID", "test-client-id")
	t.Setenv("CLIENT_SECRET", "test-client-secret")
	t.Setenv("KEY_PATH", "testdata/key.pem")
	t.Setenv("ISSUER", "https://localhost:8080")
	t.Setenv("PORT", "8080")
	t.Setenv("SCOPES", "openid profile email")
}

// parseCookie parses a raw cookie string and returns a *http.Cookie
func parseCookie(rawCookie string) *http.Cookie {

	parsedCookie := strings.Split(rawCookie, "; ")

	cookieFieldsMap := make(map[string]string)
	cookieFieldsMap["Name"] = strings.Split(parsedCookie[0], "=")[0]
	cookieFieldsMap["Value"] = strings.Split(parsedCookie[0], "=")[1]
	for _, cookieField := range parsedCookie[1:] {
		cookieFieldSplit := strings.Split(cookieField, "=")
		if len(cookieFieldSplit) != 2 {
			continue
		}
		cookieFieldsMap[cookieFieldSplit[0]] = cookieFieldSplit[1]
	}

	cookie := new(http.Cookie)
	cookie.Name = cookieFieldsMap["Name"]
	cookie.Value = cookieFieldsMap["Value"]
	cookie.Path = cookieFieldsMap["Path"]
	cookie.Expires, _ = time.Parse(time.RFC1123, cookieFieldsMap["Expires"])
	cookie.HttpOnly = cookieFieldsMap["HttpOnly"] == "true"

	return cookie
}

func TestCheckAuthorizationHeader(t *testing.T) {
	setTestEnvVars(t)

	req := httptest.NewRequest("GET", testPath, nil)

	// the header is not set
	if checkAuthorizationHeader(req.Header) {
		t.Errorf("checkAuthorizationHeader should return false")
	}

	// the header is set but the "Bearer" prefix is missing
	req.Header.Add("Authorization", idToken)
	if checkAuthorizationHeader(req.Header) {
		t.Errorf("checkAuthorizationHeader should return false")
	}

	req = httptest.NewRequest("GET", testPath, nil)

	// the header is set and the "Bearer" prefix is present
	req.Header.Add("Authorization", oidc.PrefixBearer+idToken)

	if !checkAuthorizationHeader(req.Header) {
		t.Errorf("checkAuthorizationHeader should return true")
	}
}

func TestRedirectToLoginPage(t *testing.T) {
	setTestEnvVars(t)

	e := echo.New()

	req := httptest.NewRequest("GET", testPath, nil)

	w := httptest.NewRecorder()
	c := e.NewContext(req, w)

	if err := redirectToLoginPage(c); err != nil {
		t.Errorf("redirectToLoginPage should not return an error")
	}

	if w.Header().Get("Location") != "/auth/login" {
		t.Errorf("redirectToLoginPage should return a %s location, returned %s", "/auth/login", w.Header().Get("Location"))
	}

	// check the cookie from w
	cookie := parseCookie(w.Result().Header["Set-Cookie"][0])

	if cookie.Value != testPath {
		t.Errorf("redirectToLoginPage should set a cookie with value %s, got %s", testPath, cookie.Value)
	}

	if cookie.Path != "/auth/callback" {
		t.Errorf("redirectToLoginPage should set a cookie with path %s, got %s", "/auth/callback", cookie.Path)
	}

	if cookie.Expires.IsZero() {
		t.Errorf("redirectToLoginPage should set a cookie with a non-zero expiration date")
	}

	if cookie.Expires.Sub(time.Now().Add(1*time.Minute)) > 0 {
		t.Errorf("redirectToLoginPage should set a cookie with an expiration date of at most 1 minute")
	}

}

func TestAuthorizeMiddleware(t *testing.T) {

	e := echo.New()

	// create a handler that returns a 200 status code
	testHandlerFunc := echo.HandlerFunc(func(c echo.Context) error {
		c.Response().WriteHeader(http.StatusOK)
		return nil
	})

	testHandler := authorize()(testHandlerFunc)

	req := httptest.NewRequest("GET", testPath, nil)
	w := httptest.NewRecorder()

	c := e.NewContext(req, w)
	if err := testHandler(c); err == nil {
		t.Errorf("authorize should return an error because no bearer token was provided")
	}

	req.Header.Add("Authorization", oidc.PrefixBearer+idToken)
	c = e.NewContext(req, w)

	if err := testHandler(c); err != nil {
		t.Errorf("authorize should not return an error")
	}
}

func TestAuthenticateMiddleware(t *testing.T) {
	e := echo.New()

	// create a handler that returns a 200 status code
	testHandlerFunc := echo.HandlerFunc(func(c echo.Context) error {
		c.Response().WriteHeader(http.StatusOK)
		return nil
	})

	testHandler := authenticate()(testHandlerFunc)

	req := httptest.NewRequest("GET", testPath, nil)
	w := httptest.NewRecorder()

	c := e.NewContext(req, w)
	if err := testHandler(c); err != nil {
		t.Errorf("authenticate should not return an error")
	} else if w.Header().Get("Location") != "/auth/login" {
		t.Errorf("authenticate should redirect to /auth/login, but redirected to %s", w.Header().Get("Location"))
	}

	// Test for invalid id token

	// Prepare IDToken cookie
	encodedCookie, err := secureCookie.Encode(IDTokenCookieName, idToken)
	if err != nil {
		t.Errorf("could not encode cookie: %v", err)
	}

	cookie := new(http.Cookie)
	cookie.Name = IDTokenCookieName
	cookie.Value = encodedCookie

	c.SetCookie(cookie)

	if err := testHandler(c); err != nil {
		t.Errorf("authenticate should not return an error")
	} else if w.Header().Get("Location") != "/auth/login" {
		t.Errorf("authenticate should redirect to /auth/login, but redirected to %s", w.Header().Get("Location"))
	}

}
