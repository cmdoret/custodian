# The Swiss Data Custodian documentation

The documentation is build with [Sphinx](https://www.sphinx-doc.org/en/master/) and uses the [pydata-sphynx-theme](https://pydata-sphinx-theme.readthedocs.io/en/stable/index.html).

## Setup

The package manager that is used in the repo is [pdm](https://pdm-project.org/latest/)
See [here](https://pdm-project.org/latest/#installation) for how to install it on your local computer. 
We chose pdm because it offers a way for dependency management in python that does not rely on a virtual environment.

## Build the documentation locally

Locally the documentation can be set up in the following way:

```
pdm install
pdm run build
```

The `pdm run build` command will build the html documentation in the `build` folder. 
Open the `build/index-html` with the brower of your choice and the documentation will be available locally

## Work on the Documentation

After the code changes in the `source` folder, you have two choices for the build of the documentation:

- `pdm run update`: can take care of small text changes 
- `pdm run build`: to force sphinx to rebuild from scratch

Rebuilding the documentation from scratch is necessary in case of structural changes, for example in case new elements were added on the navigation.
