**********************
Access Control System
**********************

.. image:: ../_static/images/access_control_system.png
  :width: 900
  :alt: Swiss Data Custodian Architecture: Access Control System  

Purpose
==========
The Access Control System (ACS) is the central component of the Custodian. It provides context dependent access control to resources without user authentication. 

The access control relies on two counterparts: 

The :term:`Policy Decision Point <Policy Decision Point (PDP)>` decides regarding resource access in a context dependent way and logs all access requests and decisions passing them to the :doc:`Audit Trail (AT) </architecture/at>`. 

On top of the decision making by the Custodian the external ecosystem needs to provide a :term:`Policy Enforcement Point <Policy Enforcement Point (PEP)>` to make sure the Custodian decisions are enacted in the ecosystem.  

Access 
=========
The PDP can be accessed per HTTPS API call without user authentication. It needs the names of the actors: (requester and data owner) and meta information about the data that should be accessed to make its decision.

Processing of Access Requests
=============================

The PEP sends a data access request to the PDP with the following parameters:

- the user's ID
- the processing/action (e.g., view, modify, analyze, ...)
- The data type and ID

The PDP searches for the contract in the contract knowledge base via :term:`SHACL <SHACL>` using the information provided by the PEP to find the contract that corresponds to the user, processing/action and data.

Once the contract has been found it searches for signatures in the contract signature knowledge graph. This graph stores signatures along with the ids of the contracts the signatures relate to. Once the signatures have been found, the public keys of all users who signed the contract are retrieved from the key management system. These keys are then used to verify that the signatures are valid.

In case the conditions are met and the signatures confirm that the contract is valid, the access to the resource is granted otherwise the access is denied. The decision is reported back to the PEP, which has the task to enforce the decision in the ecosystem. Additionally the request and decision are sent to the :doc:`Audit Trail (AT) </architecture/at>`, so that the event can be logged.
