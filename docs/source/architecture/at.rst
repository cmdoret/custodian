************************
Audit Trail
************************

.. image:: ../_static/images/audit_trail.png
  :width: 900
  :alt: Swiss Data Custodian Architecture: Audit Trail 

Purpose
=========

The Audit Trail (AT) has a dual task: for one it collects and stores all relevant events under the guard of the Custodian. Second it also regulates access to this monitoring database of events. 

Access 
=======

The Audit Trail has two API endpoints: one is external and the other only internal. 

The external one, the Event Resolver, requires user authentication. It allows users to access the events they are authorized to see. 

The internal API endpoint ist the Event Receiver: it collects the events from that other components: the Contract Management Systems CMS and the :doc:`Access Control System </architecture/acs>`.

Events
========

The events that are stored by the Audit Trail relate to contract creation, contract signing and access requests. 

The events have the following structure:

.. list-table::
    :widths: 10 30 10
    :header-rows: 1

    * - Field
      - Description
      - Datatype
    * - timestamp
      - Timestamp of the event
      - Unix timestamp (uint64)
    * - owner_user_ids
      - List of all the users that “own” the event
      - List of user IDs 
    * - Action_user
      - User doing the action described in the event
      - Json Object
    * - contract_uri
      - URI of the contract in the Contract Knowledge Base
      - Contract URI 
    * - resource
      - Resource accessed by the actor
      - unique resource identifier 
    * - action
      - Action to be performed on the resource (specified in the Contract Knowledge Base)
      - Action URI
    * - success
      - Was the action denied or accepted?
      - boolean
    * - message
      - Message attached to the response
      - string