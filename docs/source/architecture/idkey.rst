*****************************
Identity and Key Management
*****************************

.. image:: ../_static/images/id_and_key_management.png
  :width: 900
  :alt: Swiss Data Custodian Identity and Key Management  

Purpose
==========
The Identity and Key Management System takes care of user authentication and also stores public keys for the users that are  needed for verifying signatures on contracts.

Access
======

The Identity Management System is currently :term:`Keycloak <Keycloak>` and comes with its own API endpoints.
The Key Management System is accessible via the reverse proxy and offers two external https API endpoints to add and retrieve public keys for a user. It also offers an internal endpoint to retrieve public keys for verifying signatures on contracts.

Id Management
=============

Identity management is currently implemented with :term:`Keycloak <Keycloak>` but could be any `OpenID Connect protocol <https://openid.net/developers/how-connect-works/>`__. External identity providers can be integrated.

Key Management System
=====================

The keys are stored in `MongoDB <https://en.wikipedia.org/wiki/MongoDB>`__. Each key is a JSON object that consists of a user ID, a key ID and a public key value in the format base64. 
Public keys are used by the Custodian to verify the digital contract signatures.
