*****************
Architecture
*****************

.. toctree::
   :hidden:

   cms
   acs
   at
   idkey

.. image:: ../_static/images/custodian_architecture.png
  :width: 900
  :alt: Swiss Data Custodian Architecture   

How the Custodian works
=======================

The purpose of the Custodian is to protect and monitor :term:`resources <Resource>` in an :term:`ecosystem <Ecosystem>`. 

It makes access decisions based on contracts that are specified in a semantic way and are stored in a :term:`Contract Knowledge Graph <Contract Knowledge Graph>`.
Each access request is monitored and reported as an :term:`Audit Trail <Audit Trail (AT)>`. These audit trails can be accessed by the resource owners.
The Custodian only makes decisions, but cannot itself enforce those decisions: for that a :term:`Policy Enforcement Point <Policy Enforcement Point (PEP)>` is needed in the ecosystem.

All communication between the ecosystem and the Custodian happens via an API and a :term:`Reverse Proxy <Reverse Proxy>`. All users of the Custodian need to authenticate. For authentication currently :term:`Keycloak <Keycloak>` is used. An :term:`External Identity Provider <External Identity Provider>` from the ecosystem can be integrated.

For signing contracts additionally all users need public keys that are stored via a Key Manager.
An :term:`External Knowledge Base <External Knowledge Base>` that exists in the ecosystem can be integrated in order to hold attributes that might be needed in the access control decision.


Microservices
===============

- Reverse Proxy: Gateway to the custodian that redirects requests to the correct microservice
- :doc:`Id and Key Manager </architecture/idkey>`: Handles authentication and stores public keys of users that are needed for validating signatures
- :doc:`Contract Management System </architecture/cms>`: Manages contract creation and contract signing. All events are reported to the Audit Trail
- :doc:`Access Control System </architecture/acs>`: Decides on access request with a yes/no decision, reports all access requests and decision to the Audit Trail
- :doc:`Audit Trail </architecture/at>`: collects all events and access decision and can be accessed by resource owner to monitor the usage of the resources that they own

Workflow
===========

Contract Phase
---------------
1. :term:`Resource Owner <Resource Owner>`: The resource owner makes a contract template, that is stored in the Contract Knowledge Graph
2. :term:`Resource Owner <Resource User>`: Uses the contract template to fill in additional details that might be requested by the data owner to finalize the contract. The Resource user also signs the contract
3. :term:`Resource Owner <Resource Owner>`: The resource owner also signs the contract

Usage Phase
-----------

1. :term:`Resource Owner <Resource User>`: Request access to a resource. He receives a yes/ no response and the resource access is reported as an Audit Trail
2. :term:`Resource Owner <Resource Owner>`: The resource owner can monitor the resource usage any time by accessing the Audit Trail
