********************************
Contract Management System
********************************

.. image:: ../_static/images/contract_management_system.png
  :width: 900
  :alt: Swiss Data Custodian: Contract Management System  

Purpose
========

The CMS (Contract Management System) allows for users to manage contracts. This includes both the creation and update of contracts and also the signing of contracts. As the :doc:`Access Control System </architecture/acs>` relies on signed contracts these contracts need to be added to the system. The CMS is the component that takes care of this task.

Access 
=========

Access to the Contract Management System requires users to be authenticated. Access happens via https API endpoints. These endpoints offer services to create, update or sign contracts.

Contracts
============

The Contract Endpoint receives the contract creation requests and creates the contracts as :term:`JSON-LD <JSON-LD>` files. They are validated using :term:`SHACL <SHACL>`. The contracts are then stored in the Contract Knowledge Graph. The :doc:`Audit Trail </architecture/at>` Endpoint Event Receiver is called to store the events for the audit by the data owner.

Signatures
============

The Signature endpoint receives the contract signature requests and stores the new signatures in the Knowledge Graph.  The Audit Trail Endpoint Event Receiver is called to store the events for the audit by the data owner.
Signatures are implemented using the :term:`Elliptic Curve Digital Signature Algorithm <Elliptic Curve Digital Signature Algorithm>`. Signing a contract works by encrypting a hash of the contract with the users private key. The signature can then be verified with the users public key: decrypting the signature should match the hash of the contract. 

Interfaces with other Components
=====================================

The Contract and Signature Knowledge Graphs are outputs of this component and are both used by the ACS (Access Control System).
The Audit Trail is received by calling its internal HTTPS API endpoint the event receiver from the Contract and Signature Handlers.
