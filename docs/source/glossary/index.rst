********
Glossary
********

.. glossary::

  Access Control System (ACS)
    Access Control System: Microservices that regulates resource access

  Audit Trail (AT)
    Trail of events about the access attempts regarding a protected resource

  Contract Knowledge Graph
    RDF Graph that contains Contracts and Signatures as Semantic Data 

  Contract Management System (CMS)
    Contract Management System: Microservice that manages the contracts and signatures

  Ecosystem
    Target System for the Custodian: in which the Custodian protects :term:` resources <Resource>`

  Elliptic Curve Digital Signature Algorithm
    Algorithm that is justed by the Custodian to provide digital signatures

  External Identity Provider
    Identity Provider that already exists in the :term:`ecosystem <Ecosystem>` 

  External Knowledge Base
    Semantic Description of the Ecosystem if this exists. This can be referenced in the :term:`Contract Knowledge Graph <Contract Knowledge Graph>`

  JSON-LD
    Representation of RDF that is compatible with JSON

  Keycloak
    Authenitication System that is currently used by the Custodian, see `here <https://www.keycloak.org/>`__

  Microservice Architecture
    Robust Design Pattern for Software System, see `here <>`__`
  
  OpenID Connect
    Protocol for Authentication, see `here <https://openid.net/developers/how-connect-works/>`__  

  Policy Enforcement Point (PEP)
    Counterpart to the :term:`Policy Decision Point <Policy Decision Point (PDP)>` in the ecosystem, that enforces, what the Policy Decision Point decides  

  Policy Decision Point (PDP)
    Central part of the :term:`Access Control System <Access Control System (ACS)>` that makes the decision whether to grant access to a protected resource

  Resource
    Resource to protect by the Custodian: this can be a dataset or a service or a docker container. What it is depends on the :term:`ecosystem <Ecosystem>` 

  Resource Owner
    user in the :term:`ecosystem <Ecosystem>` who owns a protected resource and who will then have access to the :term`Audit Trail <Audit Trail (AT)>` of that resource

  Resource User
    user in the :term:`ecosystem <Ecosystem>` who is asking for access to a protected resource

  Reverse Proxy
    Gateway to the Custodian that receives the API requests and passes them on to the API Endpoints of the Custodian. See `here <https://en.wikipedia.org/wiki/Reverse_proxy>`__ for an explanation of a Reverse Proxy  

  SPARQL
    Query Language for Semantic Data

  SHACL 
    Validation Language for RDF that uses RDF to validate RDF, see `here <https://www.w3.org/TR/shacl/>`__ 
