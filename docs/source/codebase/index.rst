******************
Codebase
******************

The Code base consists of the following repositories: 

* The `Swiss Data Custodian <https://gitlab.com/data-custodian/custodian>`__  repo is programmed in Go and includes components that consist of microservices
* The `custodian-go <https://gitlab.com/data-custodian/custodian-go>`__ repo is a Go library of functions that are shared by the components: it is mounted as a git submodule the Swiss Data Custodian repo
* The `custodian-contract-interface <https://gitlab.com/data-custodian/custodian-contract-interface>`__ repo provides a UI for interactive contract creation. `Click here for a Demo <https://dev.swisscustodian.ch/>`__
* The `custodian-ontology <https://gitlab.com/data-custodian/custodian-ontology>`__ repo is written in SHACL and includes various resources related to the Custodian's semantic contracts: ontology, shapes, templates and examples.
