******************
Development
******************

How to contribute
------------------

If you are interested in contributing, please contact us! You can do this either via `email <mailto:contact@datascience.ch>`__, see `Custodian repo <https://gitlab.com/data-custodian/custodian/-/tree/main>`__ for more information about contributing.

Code of Conduct
---------------

Swiss Data Custodian is an open source project committed to creating an open, inclusive, and positive community. Please read the `Custodian Code of Conduct <https://gitlab.com/data-custodian/custodian/-/blob/main/CODE_OF_CONDUCT.mdstarting/codeofconduct.html>`__ for guidance on how to interact with others in a way that makes our community thrive.

Coding Guidelines
-----------------

To maintain consistent and well-formatted Go code across our projects, we encourage all developers to utilize Git pre-commit hooks. These hooks act as a safeguard, automatically checking and ensuring that your code adheres to the defined formatting standards before each commit.

To set up the Git pre-commit hook for Go code formatting, follow these simple steps:

1. Open your terminal.
2. Navigate to the root directory of your local custodian project.
3. Run the following command:

.. code::
    
    make pre-commit

This command will initialize the pre-commit hook, ensuring that every time you attempt to make a commit, the Go code in your changes will be checked for adherence to the specified formatting guidelines.
