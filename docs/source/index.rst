.. Swiss Data Custodian documentation master file, created by
   sphinx-quickstart on Thu Nov  2 22:13:13 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

***************************
About Swiss Data Custodian
***************************

.. image:: _static/images/custodian_intro.png
  :width: 900
  :alt: Swiss Data Custodian Architecture

About
=======

The Swiss Data Custodian provides a governance layer that can be added to an existing :term:`ecosystem <Ecosystem>`
to enforce access control policies and gather user consent for the processing of a 
user’s sensitive data.

The approach can also be generalized to add a governance layer for :term:`resource <Resource>` management 
that protects resources instead of user data.

Design
======

The Custodian adds a :term:`Policy Decision Point <Policy Decision Point (PDP)>` to an ecosystem that must be mirrored by a :term:`Policy Enforcement Point <Policy Enforcement Point (PEP)>`
in the ecosystem.

The :term:`Policy Decision Point <Policy Decision Point (PDP)>` receives requests for resource access and makes decisions to grant or deny access based on policies that have been captured in signed contracts. 

The Custodian cannot itself enforce its access decisions but needs as counterpart a :term:`Policy Enforcement Point <Policy Enforcement Point (PEP)>` in the ecosystem that protects the resources and enforces the decisions.

All events that occur in the ecosystem in regards to the protected resources are captured in an
:term:`Audit Trail <Audit Trail (AT)>` that can be viewed by the :term:`resource owners <Resource Owner>` at any time. 

The resource owner's consent is received via signing digital contracts. The legal validity of the contracts must be ensured by the ecosystem.

Technical Features
==================

The Custodian makes use of the following design patterns and technical features: 

:term:`Microservice architecture <Microservice architecture>`: 
    Microservices provide a robust modular architecture.
Interoperable context dependent contracts: 
    Contracts make use of Semantics Web technologies. Existing vocabulary in the ecosystem can be added via a Knowledge base from the ecosystem. Contracts are stored as JSON-LD in an rdf store. Contracts are validated using :term:`SHACL <SHACL>` and queried using :term:`SPARQL <SPARQL>`.
Digital signatures: 
    Contracts are digitally signed using the :term:`Elliptic Curve Digital Signature Algorithm <Elliptic Curve Digital Signature Algorithm>`.
Easy deployment: 
    The microservices are available as Helm Charts and can be deployed on a Kubernetes Cluster.
Interoperable contracts: 
    Contracts make use of Semantics Web technologies: they are implemented as :term:`JSON-LD <JSON-LD>` and validated via :term:`SHACL <SHACL>`.
Flexible Authentication*: 
    Currently :term:`Keycloak <Keycloak>` is used but could be any :term:`OpenID Connect <OpenID Connect>` enabled platform. External identity providers can be integrated.

.. toctree::
   :hidden:

   architecture/index
   ontology/index
   interfaces/index
   setup/index
   license/index
   development/index
   codebase/index
   glossary/index
