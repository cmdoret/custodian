*******************************
Setup
*******************************

The microservices can be set up via helmcharts in any Kubernetes Cluster.

Helm repo
==========

First you need to add the repo to your cluster:

.. code-block::

    helm repo add custodian "https://gitlab.com/api/v4/projects/14747939/packages/helm/devel"
    helm repo update

Helm charts
===========

The helm charts are still under construction. All available helm charts can be found 
`here <https://openid.net/developers/how-connect-w>`__.
