**************
License
**************

Copyright © 2019-2023 Swiss Data Science Center, `www.datascience.ch <www.datascience.ch>`__. All rights reserved.

The Swiss Data Custodian software is distributed as open-source under the AGPLv3 license or any later version. Details about the license can be found in the `LICENSE.AGPL <https://gitlab.com/data-custodian/custodian/-/blob/main/LICENSE.AGPL?ref_type=heads>`__ file included within the distribution package.

If the AGPLv3 license does not accommodate your project or business needs, alternative licensing options are available to meet specific requirements. These arrangements can be facilitated through the EPFL Technology Transfer Office. For more information, kindly visit their official website at `tto.epfl.ch <https://www.epfl.ch/research/technology-transfer/>`__ or direct your inquiries via email to `info.tto@epfl.ch <mailto:info.tto@epfl.ch>`__.

Please note that this software should not be used to deliberately harm any individual or entity. Users and developers must adhere to ethical guidelines and use the software responsibly and legally.

