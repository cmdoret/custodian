# The Swiss Data Custodian

## Introduction

Swiss Data Custodian is a comprehensive governance framework leveraging semantic digital contracts to ensure  secure, compliant, and transparent data sharing. With its modular architecture, the platform facilitates contract lifecycle management and precise access controls, effectively bridging the gap between privacy regulations and technical implementation.

Navigating data privacy has its intricacies, and the Swiss Data Custodian streamlines this journey with privacy-centric governance. Its capability to set policies, spanning from core infrastructure to applications and research data handling, underscores its versatility. The essence of this system lies in its fine-grained access control, effectively safeguarding sensitive assets. Here, digital semantic contracts prescribe the terms, fostering transparency and a shared understanding among all parties. Whether addressing GDPR or the Swiss Federal Act on Data Protection (nFADP) mandates, the Swiss Data Custodian clarifies the path to compliance. Comprehensive audit trails offer valuable insights into every data interaction, positioning organizations a step ahead in an ever-shifting regulatory environment.

The Swiss Data Custodian aims to unlock sensitive data value through:

- **Policy Enforcement:** Implement detailed and context-aware data handling policies, ensuring stringent privacy compliance.

- **Semantic Oversight:** Augment existing architectures with a potent semantic contract-driven layer, enhancing clarity in data management.

- **Transparent Accountability:** Capture every data touchpoint, retaining an enduring record of access and signature validations.

## Facilitating Privacy Compliance

In adherence to critical privacy protection regulations, including the Federal Act on Data Protection (FADP) and the General Data Protection Regulation (GDPR), the Swiss Data Custodian enhances privacy compliance through the following key aspects:

- **Data Subject Centricity:** Prioritize the rights and autonomy of data subjects, ensuring that they have control, transparency, and understanding of how their data is used.

- **Controller Compliance:** Equip data controllers with the tools they need to define, enforce, and verify processing activities align with the legal framework, ensuring that data subjects' rights are upheld.

- **Processor Accountability:** Streamline the responsibilities of data processors with clear contract management and audit trails, ensuring that processing is done securely, transparently, and in line with the controller's instructions.

## Getting Started

Read our documentation on [Custodian Documentation](https://data-custodian.gitlab.io/custodian/).  
Try our [Custodian Tutorial](https://data-custodian.gitlab.io/custodian/starting/index.html).

## Development Setup

If you want to build the Custodian with a working Go environment:
```bash
mkdir -p $GOPATH/src/sdsc
cd $GOPATH/src/sdsc
git clone https://gitlab.com/data-custodian/custodian.git
cd custodian
make
```

## Build the documentation

If you want to build the documentation, see [here](docs/README.md) for details

## Code of Conduct

Swiss Data Custodian is a community-driven open source project commited to creating an open, inclusive, and positive community. Please read the [Custodian Code of Conduct](https://gitlab.com/data-custodian/custodian/-/blob/main/CODE_OF_CONDUCT.md) for guidance on how to interact with others in a way that makes our community thrive.


## How to Contribute

We deeply appreciate your interest in contributing to the Swiss Data Custodian Project. Here’s a step-by-step guide to assist you through the process of contributing:

### Step 1: Sign the Appropriate CLA

Before you can contribute, you need to sign a Contributor License Agreement (CLA). The CLA clarifies the intellectual property licenses granted with contributions.

- **Individual Contributors (CLAI):** If you're contributing independently (not on behalf of a company), sign the [Individual Contributor License Agreement (CLAI)](https://gitlab.com/data-custodian/custodian/-/blob/main/CLAI.txt).
- **Corporate Contributors (CLAC):** If contributions are made by employees on behalf of a company, the company should sign the [Corporate Contributor License Agreement (CLAC)](https://gitlab.com/data-custodian/custodian/-/blob/main/CLAC.txt).

### **Step 2: Decide How to Contribute**

There are various ways to support the project:

- **Write Code:** Engage in the development process by fixing bugs or creating new features.
- **Review Pull Requests:** Review proposed changes from peers and provide constructive feedback and insights.
- **Maintain and Improve Our Website:** Enhance the project’s online visibility and usability.
- **Outreach and Onboarding:** Participate in outreach initiatives and support the onboarding process for new contributors.
- **Write Grant Proposals:** Assist with writing grant proposals and participate in other fundraising endeavors.

For minor improvements or fixes, consider opening a new issue or commenting on an existing relevant issue. If you plan to make significant contributions to the source code or would like to have a discussion before contributing, feel free to contact our coordinators at [contact@datascience.ch](mailto:contact@datascience.ch).

### **Step 3: Follow Contribution Guidelines**

For more detailed information about contributing, please visit our [Contribution Guide](https://data-custodian.gitlab.io/custodian/starting/contributing.html) on the project's [website](https://sdsc-ord.github.io).

Thank you for your interest and support! Happy contributing!

  
## License Information
  
Copyright © 2019-2023 Swiss Data Science Center, [www.datascience.ch](https://www.datascience.ch/). All rights reserved.

The Swiss Data Custodian software is distributed as open-source under the AGPLv3 license or any later version. Details about the license can be found in the `LICENSE.AGPL` file included within the distribution package.

If the AGPLv3 license does not accommodate your project or business needs, alternative licensing options are available to meet specific requirements. These arrangements can be facilitated through the EPFL Technology Transfer Office. For more information, kindly visit their official website at [tto.epfl.ch](https://tto.epfl.ch/) or direct your inquiries via email to [info.tto@epfl.ch](mailto:info.tto@epfl.ch).

Please note that this software should not be used to deliberately harm any individual or entity. Users and developers must adhere to ethical guidelines and use the software responsibly and legally.
